import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef StringCallback = Function(String value);

class SearchBar extends StatefulWidget {
  final bool showScanner;
  final StringCallback onSubmitted;

  SearchBar({Key? key, this.showScanner = false, required this.onSubmitted})
      : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final _searchController = TextEditingController();
  bool _isClearButton = false;

  @override
  void initState() {
    super.initState();
    _searchController.addListener(() {
      setState(() {
        _isClearButton = (_searchController.text.isNotEmpty);
      });
    });
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
      child: Row(
        children: [
          Expanded(
            child: CupertinoTextField(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 255, 255, 255).withOpacity(0.6),
                    spreadRadius: 1,
                    blurRadius: 1,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              placeholderStyle: TextStyle(color: Colors.black),
              controller: _searchController,
              onSubmitted: widget.onSubmitted,
              prefix: (_isClearButton)
                  ? _getClearButton()
                  : Padding(
                      padding: EdgeInsets.all(8),
                      child: Icon(
                        CupertinoIcons.search,
                        color: Colors.black,
                      ),
                    ),
            ),
          ),
          Visibility(
            visible: widget.showScanner,
            child: GestureDetector(
              onTap: () async {
                FocusManager.instance.primaryFocus?.unfocus();
              },
              child: Container(
                height: 50,
                width: 50,
                padding: const EdgeInsets.all(15),
                child: const Icon(CupertinoIcons.qrcode_viewfinder, size: 50),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget? _getClearButton() {
    if (!_isClearButton) return null;

    return Container(
      padding: const EdgeInsets.all(15),
      child: GestureDetector(
        onTap: () {
          _searchController.clear();
          widget.onSubmitted('');
        },
        child: const Icon(CupertinoIcons.clear_thick_circled,
            size: 25, color: CupertinoColors.systemGrey3),
      ),
    );
  }
}
