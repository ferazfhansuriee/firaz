import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef StringCallback = Function(String value);

class ActivityBar extends StatefulWidget {
  final bool showScanner;
  final StringCallback onSubmitted;

  ActivityBar({Key? key, this.showScanner = false, required this.onSubmitted})
      : super(key: key);

  @override
  _ActivityBarState createState() => _ActivityBarState();
}

class _ActivityBarState extends State<ActivityBar> {
  final _searchController = TextEditingController();
  bool _isClearButton = false;

  @override
  void initState() {
    super.initState();
    _searchController.addListener(() {
      setState(() {
        _isClearButton = (_searchController.text.isNotEmpty);
      });
    });
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 1),
      child: Card(
        elevation: 5,
        child: Row(
          children: [
            Expanded(
              child: CupertinoTextField(
                placeholderStyle: TextStyle(color: Colors.black),
                controller: _searchController,
                placeholder: "Tell your buds what you're doing",
                onSubmitted: widget.onSubmitted,
                prefix: (_isClearButton)
                    ? _getClearButton()
                    : Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(
                          CupertinoIcons.add,
                          color: Colors.black,
                          size: 20,
                        ),
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget? _getClearButton() {
    if (!_isClearButton) return null;

    return Container(
      padding: const EdgeInsets.all(10),
      child: GestureDetector(
        onTap: () {
          _searchController.clear();
          widget.onSubmitted('');
        },
        child: const Icon(CupertinoIcons.clear,
            size: 20, color: CupertinoColors.black),
      ),
    );
  }
}
