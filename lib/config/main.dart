class MainConfig {
  static const modeLive = false;

  static const versionHash =
      'e343ccb2c6215fa91e12e66699378f87fcd3c858d1cf8e7a96b5546ee7f33897';
  static const CURRENCY = 'RM';
  static const googleMapAPi = 'AIzaSyB73MmGeqFiWyv3fCNoUwKhPBK0mVwiHak';
  static String appUrl = '/api/v1';
}
