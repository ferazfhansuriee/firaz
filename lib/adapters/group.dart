import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GroupAdapter extends StatefulWidget {
  String? groupsName;
  String? groupsUrl;
  int? buds;
  int? index;
  GroupAdapter(
      {Key? key, this.groupsName, this.groupsUrl, this.index, this.buds})
      : super(key: key);

  @override
  State<GroupAdapter> createState() => _GroupAdapterState();
}

class _GroupAdapterState extends State<GroupAdapter> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: 180,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        color: Color.fromARGB(255, 247, 247, 247),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      height: 100,
                      width: 150,
                      child: (widget.groupsUrl != null)
                          ? Image.network(widget.groupsUrl!, fit: BoxFit.cover)
                          : Container(),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(2),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  widget.groupsName!,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  widget.buds!.toString() + " Buds",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
