import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../screens/activityprofile.dart';

class ActivityAdapter extends StatefulWidget {
  List<QueryDocumentSnapshot<Object?>> activity;
  String uid;
  int index;
  String? firstName;
  List<dynamic>? friends;

  ActivityAdapter(
      {Key? key,
      required this.activity,
      required this.uid,
      required this.index,
      this.firstName,
      this.friends})
      : super(key: key);

  @override
  State<ActivityAdapter> createState() => _ActivityAdapterState();
}

class _ActivityAdapterState extends State<ActivityAdapter> {
  bool show = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for (int i = 0; i < widget.friends!.length; i++) {
      if (widget.friends![i]['name'] ==
              widget.activity[widget.index]['group_name'] &&
          widget.friends!.length > 0 &&
          widget.firstName != widget.activity[widget.index]['admin_name']) {
        setState(() {
          show = true;
        });
      } else {
        show = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    String aPic = 'assets/images/bud' + widget.index.toString() + '.png';
    print(show);
    return Container(
        padding: const EdgeInsets.all(15),
        child: Card(
          elevation: 2,
          child: Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      height: 100,
                      width: 100,
                      child: (aPic != null)
                          ? Image.asset(aPic, fit: BoxFit.cover)
                          : Container(),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(CupertinoPageRoute(builder: (context) {
                          return ActivityProfileScreen(
                            id: widget.activity[widget.index].reference.id,
                            uid: widget.uid,
                            activityName: widget.activity[widget.index]["name"],
                            activityLocation: widget.activity[widget.index]
                                ["location"],
                            activityTime: widget.activity[widget.index]["time"],
                            activityDate: widget.activity[widget.index]["date"],
                            groupsName: widget.activity[widget.index]
                                ["group_name"],
                            firstName: widget.firstName,
                          );
                        }));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Activity: " +
                                  widget.activity[widget.index]['name'],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Group: " +
                                  widget.activity[widget.index]['group_name'],
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Admin: " +
                                  widget.activity[widget.index]['admin_name'],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Location: " +
                                  widget.activity[widget.index]['location'],
                              overflow: TextOverflow.fade,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "When: " + widget.activity[widget.index]['date'],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  CupertinoIcons.hand_raised_fill,
                                  color: Colors.black,
                                  size: 35,
                                ),
                                Icon(
                                  CupertinoIcons.heart_fill,
                                  color: Colors.black,
                                  size: 35,
                                ),
                              ],
                            )
                          ],
                        )),
                      ),
                    ),
                  )
                ],
              )),
        ));
  }
}
