import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'dart:ui' as ui;

import '../screens/activityprofile.dart';

class GoogleMapsWidget extends StatefulWidget {
  List<QueryDocumentSnapshot<Object?>> activity;
  GoogleMapsWidget({required this.activity});

  @override
  _GoogleMapsWidgetState createState() => _GoogleMapsWidgetState();
}

class _GoogleMapsWidgetState extends State<GoogleMapsWidget> {
  final _auth = FirebaseAuth.instance;
  Set<Marker> _markers = Set<Marker>();
  double? latitude;
  double? longitude;
  Completer<GoogleMapController> _controller = Completer();
  Future? future;
  List<String> _urlList = [];
  String uid = '';
  BitmapDescriptor _markerIcon = BitmapDescriptor.defaultMarker;
  bool isLoading = false;
  @override
  void initState() {
    super.initState();

    _getPermission();
    future = _getLocation();
  }

  void dispose() {
    super.dispose();
    _urlList.clear();
  }

  _getPermission() {
    Permission.locationWhenInUse.request();
  }

  Future<void> _getLocation() async {
    final User user = _auth.currentUser!;
    uid = user.uid.toString();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      latitude = position.latitude;
      longitude = position.longitude;
    });
    print(latitude);
    print(longitude);
    _markers.add(Marker(
      flat: false,
      markerId: MarkerId("current"),
      position: LatLng(latitude!, longitude!),
      infoWindow: InfoWindow(
        title: "Your Location",
      ),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
    ));
    print(widget.activity.length);
    for (int i = 0; i < widget.activity.length; i++) {
      _markers.add(
        Marker(
          flat: false,
          markerId: MarkerId("$i"),
          position: LatLng(double.parse(widget.activity[i]['latitude']!),
              double.parse(widget.activity[i]['longitude']!)),
          infoWindow: InfoWindow(
              title: widget.activity[i]['name']!,
              snippet: widget.activity[i]['location'],
              onTap: () {
                Navigator.of(context)
                    .push(CupertinoPageRoute(builder: (context) {
                  return ActivityProfileScreen(
                    id: widget.activity[i].reference.id,
                    uid: uid,
                    activityName: widget.activity[i]["name"],
                    activityLocation: widget.activity[i]["location"],
                    activityTime: widget.activity[i]["time"],
                    activityDate: widget.activity[i]["date"],
                    groupsName: widget.activity[i]["group_name"],
                    firstName: uid,
                  );
                }));
              }),
          icon: BitmapDescriptor.defaultMarker,
        ),
      );
      if (_markers.length >= 1) {
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      }
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 75 / 100,
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              // decoration: BoxDecoration(
              //     border: Border.all(width: 2, color: Colors.black)),
              child: FutureBuilder(
                  future: future,
                  builder: (context, snapshot) {
                    return GoogleMap(
                      myLocationEnabled: true,
                      myLocationButtonEnabled: true,
                      mapType: MapType.normal,
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: CameraPosition(
                          target: LatLng(latitude!, longitude!), zoom: 11),
                      markers: _markers,
                    );
                  })),
        ],
      ),
    );
  }
}
