import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/screens/friend.dart';

import '../screens/activityprofile.dart';

class ActivityAdapter2 extends StatefulWidget {
  List<QueryDocumentSnapshot<Object?>> activity;
  String uid;
  int index;
  int? bud;
  String? firstName;
  List<dynamic>? friends;

  ActivityAdapter2(
      {Key? key,
      required this.activity,
      required this.uid,
      required this.index,
      this.bud,
      this.firstName,
      this.friends})
      : super(key: key);

  @override
  State<ActivityAdapter2> createState() => _ActivityAdapter2State();
}

class _ActivityAdapter2State extends State<ActivityAdapter2> {
  bool show = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String aPic = 'assets/images/bud' + widget.index.toString() + '.png';

    return Container(
      height: 125,
      width: 180,
      child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.activity[widget.index]["name"],
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 16, color: Colors.black),
                ),
                Text(
                  widget.activity[widget.index]['group_name'],
                  style: TextStyle(fontSize: 16, color: Colors.black),
                ),
                Text(
                  widget.activity[widget.index]['location'],
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 1),
                  child: Divider(
                    thickness: 1,
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
