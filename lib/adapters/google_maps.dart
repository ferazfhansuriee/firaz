import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'package:permission_handler/permission_handler.dart';

class GoogleMapsWidget extends StatefulWidget {
  final String? name;
  final String? latitude;
  final String? longitude;
  GoogleMapsWidget({this.name, this.latitude, this.longitude});

  @override
  _GoogleMapsWidgetState createState() => _GoogleMapsWidgetState();
}

class _GoogleMapsWidgetState extends State<GoogleMapsWidget> {
  Set<Marker> _markers = Set<Marker>();
  BitmapDescriptor _markerIcon = BitmapDescriptor.defaultMarker;
  double? latitudeCaptured;
  double? longitudeCaptured;
  double? latitudeStored;
  double? longitudeStored;
  Completer<GoogleMapController> _controller = Completer();
  Future? future;

  @override
  void initState() {
    super.initState();
    _getPermission();
    future = _getLocation();
  }

  _getPermission() {
    Permission.locationWhenInUse.request();
  }

  Future<void> _getLocation() async {
    Future.delayed(Duration(seconds: 0), () async {
      setState(() {
        latitudeStored = double.parse(widget.latitude!);
        longitudeStored = double.parse(widget.longitude!);
      });

      setState(() {
        latitudeCaptured = (widget.latitude!.isNotEmpty)
            ? double.parse(widget.latitude!)
            : null;
        longitudeCaptured = (widget.longitude!.isNotEmpty)
            ? double.parse(widget.longitude!)
            : null;

        if (this.mounted) {
          _markers.add(
            Marker(
                markerId: MarkerId("0"),
                position: LatLng(
                    (latitudeCaptured == null)
                        ? latitudeStored!
                        : latitudeCaptured!,
                    (longitudeCaptured == null)
                        ? longitudeStored!
                        : longitudeCaptured!),
                infoWindow: InfoWindow(
                  title: widget.name,
                ),
                icon: _markerIcon),
          );
        }
      });
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 300,
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            // decoration: BoxDecoration(
            //     border: Border.all(width: 2, color: Colors.black)),
            child: FutureBuilder(
                future: future,
                builder: (context, snapshot) {
                  print(latitudeCaptured);
                  print(longitudeCaptured);
                  if (latitudeStored != null && longitudeStored != null ||
                      latitudeCaptured != null && longitudeCaptured != null) {
                    return GoogleMap(
                      mapType: MapType.normal,
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: CameraPosition(
                        target: LatLng(
                            (latitudeCaptured == null)
                                ? latitudeStored!
                                : latitudeCaptured!,
                            (longitudeCaptured == null)
                                ? longitudeStored!
                                : longitudeCaptured!),
                        zoom: 16,
                      ),
                      markers: _markers,
                    );
                  }
                  return LinearProgressIndicator();
                }),
          ),
        ],
      ),
    );
  }
}
