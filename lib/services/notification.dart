import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:google_api_availability/google_api_availability.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationService {
  String? mtoken = " ";
  NotificationDetails? notificationDetails;
  static final NotificationService _notificationService =
      NotificationService._internal();

  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  Future<void> init() async {
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('ic_launcher');

    const IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
    );

    const InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );

    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String? payload) async {
      if (payload != null) {
        Map<String, dynamic> data = jsonDecode(payload);
        gotoNotification(data);
      }
    });

    if (Platform.isIOS) {
      const IOSNotificationDetails iOSPlatformChannelSpecifics =
          IOSNotificationDetails(
        presentAlert: true,
        presentBadge: false,
        presentSound: true,
      );
      notificationDetails =
          const NotificationDetails(iOS: iOSPlatformChannelSpecifics);
    } else if (Platform.isAndroid) {
      const AndroidNotificationDetails androidPlatformChannelSpecifics =
          AndroidNotificationDetails(
        'default',
        'Default Channel',
        channelDescription: 'Default Channel',
        importance: Importance.high,
      );
      notificationDetails =
          const NotificationDetails(android: androidPlatformChannelSpecifics);
    }
  }

  Future<String?> getFirebaseMessagingToken() async {
    await FirebaseMessaging.instance.getToken().then((token) {
      print("working");
      mtoken = token;
    });
    return mtoken;
  }

  Future<void> gotoNotification(Map<String, dynamic> data) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.containsKey('api.user_id')) {
      // TODO: Navigate to NotificationScreen
    }
  }
}
