import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:jom/screens/bud.dart';
import 'package:jom/screens/dashboard.dart';
import 'package:jom/screens/home.dart';
import 'package:jom/services/database.dart';

import '../utils/toast.dart';

class AuthenticationService {
  final FirebaseAuth _firebaseAuth;

  AuthenticationService(this._firebaseAuth);

  /// Changed to idTokenChanges as it updates depending on more cases.
  Stream<User?> get authStateChanges => _firebaseAuth.idTokenChanges();

  /// This won't pop routes so you could do something like

  /// after you called this method if you want to pop all routes.
  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  /// There are a lot of different ways on how you can do exception handling.
  /// This is to make it as easy as possible but a better way would be to
  /// use your own custom class that would take the exception and return better
  /// error messages. That way you can throw, return or whatever you prefer with that instead.
  Future<String> signIn(
      {required BuildContext context, String? email, String? password}) async {
    try {
      UserCredential result = await _firebaseAuth.signInWithEmailAndPassword(
          email: email!, password: password!);
      if (result != null) {
        Navigator.of(context).push(CupertinoPageRoute(builder: (context) {
          return DashboardScreen();
        }));
      }
      return "Signed in";
    } on FirebaseAuthException catch (e) {
      return "error";
    }
  }

  /// There are a lot of different ways on how you can do exception handling.
  /// This is to make it as easy as possible but a better way would be to
  /// use your own custom class that would take the exception and return better
  /// error messages. That way you can throw, return or whatever you prefer with that instead.

  static Future<void> signOutGoogle({required BuildContext context}) async {
    final GoogleSignIn googleSignIn = GoogleSignIn();

    try {
      if (!kIsWeb) {
        await googleSignIn.signOut();
      }
      await FirebaseAuth.instance.signOut();
    } catch (e) {
      Toast.show(context, "danger", e.toString());
    }
  }

  static Future<User?> signUpWithGoogle({
    required BuildContext context,
    String? token,
  }) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    User? user;

    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      try {
        final UserCredential userCredential =
            await auth.signInWithCredential(credential);

        user = userCredential.user;
      } on FirebaseAuthException catch (e) {
        if (e.code == 'account-exists-with-different-credential') {
          Toast.show(context, "danger", "Account already exist");
        } else if (e.code == 'invalid-credential') {
          Toast.show(context, "danger", "Invalid");
        }
      } catch (e) {
        Toast.show(context, "danger", e.toString());
      }
    }

    return user;
  }

  static Future<User?> signInWithGoogle({
    required BuildContext context,
    String? token,
  }) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    User? user;

    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      try {
        final UserCredential userCredential =
            await auth.signInWithCredential(credential);

        user = userCredential.user;
        if (userCredential.additionalUserInfo!.isNewUser) {
          Navigator.of(context).push(CupertinoPageRoute(builder: (context) {
            return BudFormScreen();
          }));
        } else {
          print("hello");
          Navigator.pushReplacementNamed(context, '/home');
        }
      } on FirebaseAuthException catch (e) {
        if (e.code == 'account-exists-with-different-credential') {
          Toast.show(context, "danger", "Account already exist");
        } else if (e.code == 'invalid-credential') {
          Toast.show(context, "danger", "Invalid");
        }
      } catch (e) {
        Toast.show(context, "danger", e.toString());
      }
    }

    return user;
  }
}
