import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jom/models/user.dart';

class DatabaseService {
  DatabaseService();

  // collection reference
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('user');

  final CollectionReference groupCollection =
      FirebaseFirestore.instance.collection('group');
  final CollectionReference activitiesCollection =
      FirebaseFirestore.instance.collection('activities');
  final CollectionReference notificationCollection =
      FirebaseFirestore.instance.collection('notification');
  List user = [];
  Future<int> getFriend() async {
    // Get docs from collection reference
    QuerySnapshot querySnapshot = await userCollection.get();

    // Get data from docs and convert map to List
    final allData = querySnapshot.docs.map((doc) => doc.data()).toList();
    return allData.length;
  }

  Future<int> getActivity() async {
    // Get docs from collection reference
    QuerySnapshot querySnapshot = await groupCollection.get();

    // Get data from docs and convert map to List
    final allData = querySnapshot.docs.map((doc) => doc.data()).toList();
    return allData.length;
  }

  Future getData() async {
    try {
      //to get data from a single/particular document alone.
      // var temp = await collectionRef.doc("<your document ID here>").get();

      // to get data from all documents sequentially
      await userCollection.get().then((querySnapshot) {
        for (var result in querySnapshot.docs) {
          user.add(result.data());
        }
      });

      return user;
    } catch (e) {
      print("Error - $e");
      return e;
    }
  }

  Future<void> registerUserData(String name, String token, int bud) async {
    return await userCollection.doc(name).set({
      'name': name,
      'token': token,
      'bud': bud,
    });
  }

  Future<void> updateUserData(String uid, int bud) async {
    return await userCollection.doc(uid).update({
      'bud': bud,
    });
  }

  Future<void> addUserBud(
    int bud,
    String uid,
  ) async {
    return await userCollection.doc(uid).update({
      'bud': bud,
    });
  }

  Future<void> addPendingFriend(String uid, String name, String id,
      String messagingToken, int isAccept) async {
    return await userCollection.doc(uid).update({
      "friends": FieldValue.arrayUnion([
        {
          "name": name,
          "id": id,
          "token": messagingToken,
          "status": "Pending",
        },
      ])
    });
  }

  Future<void> addFriend(
    String name,
    String firendName,
    String token,
  ) async {
    return await userCollection.doc(name).update({
      "friends": FieldValue.arrayUnion([
        {
          'name': firendName,
          'token': token,
        },
      ])
    });
  }

  Future<void> addFriend2(
    String name,
    String firendName,
    String token,
  ) async {
    print(firendName);
    return await userCollection.doc(firendName).update({
      "friends": FieldValue.arrayUnion([
        {
          "name": name,
          "token": token,
        },
      ])
    });
  }

  Future<void> removeFriend(String uid, String name, String id,
      String messagingToken, int isAccept) async {
    return await userCollection.doc(uid).collection("friends").doc(id).delete();
  }

  Future<void> addActivity(
      String admin_name,
      String name,
      String date,
      String time,
      String location,
      String latitude,
      String longitude,
      String url,
      bool isPublic,
      String group_name) async {
    return await groupCollection.doc(group_name).update({
      "activities": FieldValue.arrayUnion([
        {
          "admin_name": admin_name,
          "name": name,
          'date': date,
          'time': time,
          'location': location,
          'latitude': latitude,
          'longitude': longitude,
          'url': url,
          'joined': 1,
          'isPublic': isPublic,
          'group_name': group_name
        },
      ])
    });
  }

  Future<void> createActivity(
      String admin_name,
      String name,
      String date,
      String time,
      String location,
      String latitude,
      String longitude,
      String url,
      bool isPublic,
      String group_name) async {
    return await activitiesCollection.doc(name).set({
      "admin_name": admin_name,
      "name": name,
      'date': date,
      'time': time,
      'location': location,
      'latitude': latitude,
      'longitude': longitude,
      'url': url,
      'joined': 1,
      'isPublic': isPublic,
      'group_name': group_name,
      "members": FieldValue.arrayUnion([
        {
          "name": admin_name,
        },
      ])
    });
  }

  Future<void> addGroup(String firstName, String name, String url) async {
    return await userCollection.doc(firstName).update({
      "groups": FieldValue.arrayUnion([
        {
          "name": name,
          'url':
              (url != "") ? url : "gs://jom-firaz.appspot.com/images/logo.png",
        },
      ])
    });
  }

  Future<void> createGroup(String name, String firstname, String url) async {
    return await groupCollection.doc(name).set({
      'admin': firstname,
      'group_name': name,
      'url': (url != "") ? url : "gs://jom-firaz.appspot.com/images/logo.png",
      "members": FieldValue.arrayUnion([
        {
          "name": firstname,
        },
      ])
    });
  }

  Future<void> deleteGroup(
      String uid, String name, String time, String location, String url) async {
    return await userCollection.doc(uid).update({
      "groups": FieldValue.arrayRemove([
        {"name": name, 'time': time, 'location': location, 'url': url},
      ])
    });
  }

  Future<void> deletegroup(
    String id,
  ) async {
    return await groupCollection.doc(id).delete();
  }

  Future<DocumentReference<Object?>> createNotification(String uid, String id,
      String firstName, String firendName, String token, String title) async {
    return await notificationCollection.add({
      'sender_name': firstName,
      'reciever_name': firendName,
      'reciever_token': token,
      'message': title,
    });
  }

  Future<void> deleteNotification(
    String id,
  ) async {
    return await notificationCollection.doc(id).delete();
  }

  Future<DocumentReference<Object?>> activityNotification(
      String firstName,
      String firendName,
      String token,
      String title,
      String adName,
      String gName) async {
    return await notificationCollection.add({
      'sender_name': firstName,
      'reciever_name': firendName,
      'reciever_token': token,
      'message': title,
      'adName': adName,
      'gName': gName,
    });
  }

  Future<DocumentReference<Object?>> memberNotification(
    String firstName,
    String firendName,
    String token,
    String title,
    String gname,
  ) async {
    return await notificationCollection.add({
      'sender_name': firstName,
      'reciever_name': firendName,
      'reciever_token': token,
      'message': title,
      'group_name': gname,
    });
  }

  Future<void> updateJoin(String aName, String name) async {
    return await activitiesCollection.doc(aName).update({
      "members": FieldValue.arrayUnion([
        {
          "name": name,
        },
      ]),
      "joined": FieldValue.increment(1),
    });
  }

  Future<void> addMember(String groupName, String name, String token) async {
    return await groupCollection.doc(groupName).update({
      "members": FieldValue.arrayUnion([
        {"name": name, "token": token},
      ])
    });
  }

  Future<void> addActivityMember(
      String aName, String name, String token) async {
    return await activitiesCollection.doc(aName).update({
      "members": FieldValue.arrayUnion([
        {
          "name": name,
        },
      ])
    });
  }
}
