import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:jom/screens/activitylist.dart';
import 'package:jom/screens/activitymap.dart';
import 'package:jom/screens/groupadd.dart';
import 'package:jom/screens/groupprofile.dart';
import 'package:jom/screens/friendadd.dart';
import 'package:jom/screens/friendprofile.dart';
import 'package:jom/screens/notification.dart';
import 'package:jom/screens/register_form.dart';
import 'screens/splash.dart';
import 'screens/login.dart';
import 'screens/home.dart';
import 'services/notification.dart';

Future<void> main() async {
  debugPaintSizeEnabled = false;

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.dark,
  ));

  // TODO: Push Notification
  // // Firebase Cloud Messaging (FCM)

  WidgetsFlutterBinding.ensureInitialized();
  await NotificationService().init();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return CupertinoApp(
      title: 'Buds',
      debugShowCheckedModeBanner: false,
      theme: const CupertinoThemeData(
        brightness: Brightness.light,
        scaffoldBackgroundColor: CupertinoColors.lightBackgroundGray,
        primaryColor: Color.fromARGB(255, 0, 0, 0),
        textTheme: CupertinoTextThemeData(
          primaryColor: Color.fromARGB(255, 0, 0, 0),
          textStyle: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 14,
              color: Color.fromARGB(255, 104, 104, 104),
              fontWeight: FontWeight.normal),
        ),
      ),
      localizationsDelegates: const [
        DefaultWidgetsLocalizations.delegate,
        DefaultMaterialLocalizations.delegate,
      ],
      initialRoute: '/',
      routes: {
        '/': (context) => const SplashScreen(),
        '/home': (context) => const Home(),
        '/login': (context) => const LoginScreen(),
        '/register': (context) => const RegisterFormScreen(),
        '/friend-form': (context) => const FriendFormScreen(),
        '/groups-form': (context) => GroupsFormScreen(),
        '/groups-profile': (context) => GroupsProfileScreen(),
        '/notifications': (context) => NotificationScreen(),
        '/activities': (context) => ActivityMapScreen(),
      },
    );
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}
