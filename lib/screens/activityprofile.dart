import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_place/google_place.dart';
import 'package:intl/intl.dart';
import 'package:jom/adapters/google_maps.dart';
import 'package:map_launcher/map_launcher.dart';

import '../services/database.dart';
import '../services/notification.dart';
import '../utils/toast.dart';

class ActivityProfileScreen extends StatefulWidget {
  dynamic groups;
  String? uid;
  late String? groupsName;
  String? groupsUrl;
  String? id;
  String? groupDesc;
  late int? guest;
  String? firstName;
  String? activityName;
  String? activityLocation;
  String? activityTime;
  String? activityDate;
  ActivityProfileScreen(
      {Key? key,
      this.groups,
      this.groupsName,
      this.guest,
      this.groupDesc,
      this.id,
      this.groupsUrl,
      this.uid,
      this.activityName,
      this.activityTime,
      this.activityLocation,
      this.activityDate,
      this.firstName})
      : super(key: key);

  @override
  _ActivityProfileScreenState createState() => _ActivityProfileScreenState();
}

class _ActivityProfileScreenState extends State<ActivityProfileScreen> {
  final _auth = FirebaseAuth.instance;
  String id = '';
  String adminName = '';
  List<dynamic> members = [];
  List<dynamic> activities = [];
  int join = 0;
  String activityName = "";
  String groupName = "";
  String location = "";
  String date = "";
  String time = "";
  bool member = false;
  LatLng? position;
  Coords? coords;
  CameraPosition? camera;
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    id = widget.id!;
    print(widget.id);
    DateTime format = DateTime.parse(widget.activityDate!);
    DateTime format2 =
        DateTime.parse(widget.activityDate! + " " + widget.activityTime!);
    date = DateFormat.MMMMEEEEd().format(format);
    time = DateFormat.jms().format(format2).substring(2);
    print(time);
    print(format2);
    print(widget.groupsName);
    Future.delayed(Duration.zero, () {
      getMembers();
    });
    super.initState();
  }

  Future<void> _showMap() async {
    final availableMaps = await MapLauncher.installedMaps;
    print(
        availableMaps); // [AvailableMap { mapName: Google Maps, mapType: google }, ...]
    await _showApps(context, availableMaps);
  }

  _showApps(BuildContext context, List<AvailableMap> maps) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {},
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Select app",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      content: Container(
        height: 180,
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            padding:
                EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
            physics: const AlwaysScrollableScrollPhysics(),
            itemCount: maps.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () async {
                  await maps[index].showMarker(
                    coords: coords!,
                    title: location,
                  );
                  print(maps[index].icon);
                },
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      SvgPicture.asset(
                        maps[index].icon,
                        height: 50.0,
                        width: 50.0,
                      ),
                      Text(maps[index].mapName)
                    ],
                  ),
                ),
              );
            }),
      ),
    );

    // show the dialog
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> getMembers() async {
    if (members.isEmpty) {
      await FirebaseFirestore.instance
          .collection("activities")
          .doc(id)
          .get()
          .then((value) {
        setState(() {
          activityName = value.get("name");
          join = value.get("joined");
          adminName = value.get('admin_name');
          members = value.get("members");
          coords = Coords(double.parse(value.get("latitude")),
              double.parse(value.get("longitude")));
          position = LatLng(double.parse(value.get("latitude")),
              double.parse(value.get("longitude")));
          print(position);
        });

        if (members.isNotEmpty) {
          for (int i = 0; i < members.length; i++) {
            if (widget.firstName == members[i]['name']) {
              setState(() {
                member = true;
              });
            }
          }
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(255, 255, 255, 255)
                                .withOpacity(0.6),
                            spreadRadius: 1,
                            blurRadius: 1,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 15.0),
                          child: Text(
                            activityName,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 28,
                            ),
                          ),
                        ),
                        Divider(color: CupertinoColors.separator, height: 1),
                        if (position != null)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Card(
                              child: Container(
                                height: 300,
                                width: 300,
                                child: GoogleMapsWidget(
                                  name: widget.activityName,
                                  latitude: position!.latitude.toString(),
                                  longitude: position!.longitude.toString(),
                                ),
                              ),
                            ),
                          ),
                        Padding(
                          padding: EdgeInsets.all(5),
                          child: GestureDetector(
                            onTap: () {
                              _showMap();
                            },
                            child: Icon(
                              CupertinoIcons.map_fill,
                              size: 30,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Text(
                                widget.activityLocation!,
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Icon(CupertinoIcons.calendar),
                                  Text(
                                    date,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: 100,
                              ),
                              Row(
                                children: [
                                  Icon(CupertinoIcons.clock),
                                  Text(
                                    time,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Divider(color: CupertinoColors.separator, height: 1),
                        Padding(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Buds that are going ",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Container(
                                          height: 100,
                                          child: ListView.builder(
                                              shrinkWrap: true,
                                              scrollDirection: Axis.horizontal,
                                              padding: EdgeInsets.only(
                                                  bottom: MediaQuery.of(context)
                                                      .padding
                                                      .bottom),
                                              physics:
                                                  const AlwaysScrollableScrollPhysics(),
                                              itemCount: join,
                                              itemBuilder: (context, index) {
                                                int num = index + 2;
                                                return Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 20,
                                                      vertical: 5),
                                                  child: Column(
                                                    children: [
                                                      Image.asset(
                                                        'assets/images/bud$num.png',
                                                        height: 45,
                                                      ),
                                                      Text(members[index]
                                                          ['name'])
                                                    ],
                                                  ),
                                                );
                                              }),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        if (adminName != widget.firstName && member == false)
                          Container(
                            decoration: BoxDecoration(
                              color: CupertinoTheme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(25),
                            ),
                            width: 150,
                            height: 36,
                            child: SizedBox(
                              child: CupertinoButton(
                                disabledColor: CupertinoColors.inactiveGray,
                                padding: EdgeInsets.zero,
                                onPressed: () async {
                                  _activityNotification(context);
                                },
                                child: const Text('Join',
                                    style: TextStyle(
                                        color: CupertinoColors.white)),
                              ),
                            ),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(5),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              border: Border.all(color: Colors.black),
                              boxShadow: [
                                BoxShadow(
                                  color: CupertinoColors.lightBackgroundGray,
                                  spreadRadius: 1,
                                  blurRadius: 1,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            width: 150,
                            height: 36,
                            child: SizedBox(
                              child: CupertinoButton(
                                disabledColor: CupertinoColors.inactiveGray,
                                padding: EdgeInsets.zero,
                                onPressed: () async {
                                  Navigator.pop(context);
                                },
                                child: const Text('Back',
                                    style: TextStyle(
                                        color: CupertinoColors.black)),
                              ),
                            ),
                          ),
                        ),
                      ]),
                    )
                  ]),
            )));
  }

  Future<void> _activityNotification(BuildContext context) async {
    String uid = _auth.currentUser!.uid;
    String token = "";
    String aid = widget.id!;
    groupName = widget.groupsName!;
    String reciever_name = adminName;
    String sender_name = _auth.currentUser!.displayName!;
    List<String> errors = [];

    if (errors.isEmpty) {
      String? messagingToken =
          await NotificationService().getFirebaseMessagingToken();

      final GlobalKey progressDialogKey = GlobalKey<State>();
      FirebaseFirestore.instance
          .collection("user")
          .where("name", isEqualTo: adminName)
          .get()
          .then((querySnapshot) async {
        if (!querySnapshot.docs.isEmpty) {
          token = querySnapshot.docs[0].get("token");
          reciever_name = querySnapshot.docs[0].get("name");
          await DatabaseService().activityNotification(
              sender_name,
              reciever_name,
              token,
              "Activity Request",
              activityName,
              groupName);
          Toast.show(context, 'success', "Request Sent");
          Navigator.pushReplacementNamed(context, '/home');
        } else {
          Toast.show(context, 'danger', "Friend not found");
          print("Document Doesn't Exist");
        }
      });
    }

    if (errors.isNotEmpty) {
      Toast.show(context, 'danger', errors[0]);
    }
  }
}
