import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/adapters/p_activity.dart';
import 'package:jom/screens/activitymap.dart';
import 'package:jom/screens/friendprofile.dart';

import '../widgets/search_bar.dart';
import 'activityprofile.dart';

class ActivityListScreen extends StatefulWidget {
  const ActivityListScreen({Key? key}) : super(key: key);

  @override
  _ActivityListScreenState createState() => _ActivityListScreenState();
}

class _ActivityListScreenState extends State<ActivityListScreen> {
  final _auth = FirebaseAuth.instance;
  String firstName = '';
  String uid = '';
  String mtoken = "";
  List<dynamic> groups = [];
  List<dynamic> members = [];
  List<dynamic> activities = [];
  @override
  void initState() {
    getUser();
    super.initState();
  }

  void getUser() {
    final User user = _auth.currentUser!;
    uid = user.uid.toString();
    FirebaseFirestore.instance.collection("user").doc(uid).get().then((value) {
      if (value.exists) {
        setState(() {
          firstName = value.get("name");
          groups = value.get("groups");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: Column(children: [
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, '/groups-form');
          },
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "What your buds are doing",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                  ),
                ),
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.popUntil(context, (route) => route.isFirst);
                    Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (context) {
                      return ActivityMapScreen();
                    }));
                  },
                  child: Icon(
                    Icons.map_outlined,
                    size: 40,
                  ))
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height - 150,
          child: StreamBuilder<QuerySnapshot>(
            stream:
                FirebaseFirestore.instance.collection("activities").snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                  ),
                );
              } else {
                final docs = snapshot.data!.docs;

                return Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: SizedBox(
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      physics: const AlwaysScrollableScrollPhysics(),
                      itemCount: docs.length,
                      itemBuilder: (context, index) {
                        String gId =
                            (docs.isNotEmpty) ? docs[index].reference.id : "";
                        return ActivityAdapter(
                            activity: docs,
                            uid: uid,
                            index: index,
                            firstName: firstName,
                            friends: groups);
                      },
                    ),
                  ),
                );
              }
            },
          ),
        ),
      ]),
    ));
  }
}
