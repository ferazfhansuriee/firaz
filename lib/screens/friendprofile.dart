import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/services/auth_service.dart';

class FriendProfileScreen extends StatefulWidget {
  dynamic friend;
  late String friendName;
  String? friendUrl;
  FriendProfileScreen(
      {Key? key, this.friend, required this.friendName, this.friendUrl})
      : super(key: key);

  @override
  _FriendProfileScreenState createState() => _FriendProfileScreenState();
}

class _FriendProfileScreenState extends State<FriendProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(children: [
            Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Image.asset(
                        (widget.friendUrl != null)
                            ? widget.friendUrl!
                            : 'assets/images/logo3.png',
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Text(
                        widget.friendName,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                )),
            Column(
              children: [
                Divider(color: CupertinoColors.separator, height: 1),
                Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Who is " + widget.friendName),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(color: Colors.black)),
                          child: Padding(
                            padding: const EdgeInsets.all(50),
                            child: Text("About me"),
                          ),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              CupertinoIcons.hand_raised,
                              size: 50,
                            ),
                            Text("Wave")
                          ],
                        ),
                      ],
                    )),
                Divider(color: CupertinoColors.separator, height: 1),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Container(
                    decoration: BoxDecoration(
                      color: CupertinoTheme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(25),
                    ),
                    width: 150,
                    height: 36,
                    child: SizedBox(
                      child: CupertinoButton(
                        disabledColor: CupertinoColors.inactiveGray,
                        padding: EdgeInsets.zero,
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        child: const Text('Back',
                            style: TextStyle(color: CupertinoColors.white)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ])),
    ));
  }
}
