import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jom/screens/dashboard.dart';
import 'package:jom/services/auth_service.dart';
import 'package:jom/services/database.dart';
import '../screens/register_form.dart';
import '../services/notification.dart';
import '../utils/progress_dialog.dart';
import '../utils/toast.dart';
import '../utils/web_service.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  void init() {
    getToken();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: CupertinoPageScaffold(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: Container(
              width: double.infinity,
              decoration: const BoxDecoration(),
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Text(
                          "Welcome Bud",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 25,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 70, bottom: 30),
                        child: Image.asset('assets/images/logo3.png',
                            fit: BoxFit.contain, width: 200),
                      ),
                      Container(
                        height: 46,
                        width: 250,
                        padding: const EdgeInsets.symmetric(vertical: 4),
                        child: CupertinoTextField(
                          key: const Key("username"),
                          enableSuggestions: false,
                          autocorrect: false,
                          controller: _usernameController,
                          placeholder: 'Username',
                          prefix: const Padding(
                            padding: EdgeInsets.all(8),
                            child: Icon(Icons.person),
                          ),
                        ),
                      ),
                      Container(
                        height: 46,
                        width: 250,
                        padding: const EdgeInsets.symmetric(vertical: 4),
                        child: CupertinoTextField(
                          key: const Key("password"),
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                          controller: _passwordController,
                          placeholder: 'Password',
                          prefix: const Padding(
                            padding: EdgeInsets.all(8),
                            child: Icon(Icons.lock),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 15),
                        width: 150,
                        height: 36,
                        child: CupertinoButton(
                          key: Key('login_button'),
                          color: Colors.black,
                          disabledColor: CupertinoColors.inactiveGray,
                          padding: EdgeInsets.zero,
                          onPressed: () {
                            _login(context);
                          },
                          child: Text('Log in',
                              style: const TextStyle(
                                fontFamily: 'Montserrat',
                                color: CupertinoColors.white,
                              )),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text('Google Account',
                          style: TextStyle(
                              color: CupertinoTheme.of(context).primaryColor)),
                      GestureDetector(
                        onTap: () {
                          _googleSignIn(context);
                        },
                        child: SizedBox(
                          width: 75,
                          height: 75,
                          child: Image.network(
                              'http://pngimg.com/uploads/google/google_PNG19635.png',
                              fit: BoxFit.cover),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        child: CupertinoButton(
                          padding: EdgeInsets.zero,
                          onPressed: () {},
                          child: Text('Need help? ',
                              style: TextStyle(
                                  color:
                                      CupertinoTheme.of(context).primaryColor)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }

  void getToken() async {}
  Future<void> _login(BuildContext context) async {
    print(_usernameController.text);
    String username = _usernameController.text;
    final user = await _auth.signInWithEmailAndPassword(
        email: username, password: _passwordController.text);
    if (user != null) {
      Navigator.pushNamed(context, 'home_screen');
    }
  }

  Future<void> _googleSignIn(BuildContext context) async {
    String? messagingToken =
        await NotificationService().getFirebaseMessagingToken();
    await AuthenticationService.signInWithGoogle(
        context: context, token: messagingToken);
  }
}
