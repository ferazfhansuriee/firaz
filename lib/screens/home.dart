import 'dart:async';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:jom/screens/activitylist.dart';
import 'package:jom/screens/activitymap.dart';
import 'package:jom/screens/groupadd.dart';
import 'package:jom/screens/dashboard.dart';
import 'package:jom/screens/friend.dart';
import 'package:jom/screens/notification.dart';
import 'package:jom/screens/profile.dart';
import 'package:jom/services/database.dart';
import 'package:uni_links/uni_links.dart';
import '../global.dart' as global;
import '../utils/toast.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late StreamSubscription _streamSubscription;
  String? mtoken = " ";

  @override
  void initState() {
    super.initState();

    _requestPushNotificationPermission();
    _initUniLinks();

    global.homeState = this;
  }

  @override
  void dispose() {
    _streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          iconSize: 20,
          inactiveColor: CupertinoColors.white,
          activeColor: Color.fromARGB(255, 80, 80, 80),
          backgroundColor: CupertinoTheme.of(context).primaryColor,
          onTap: (index) {
            final FirebaseAnalytics firebaseAnalytics =
                FirebaseAnalytics.instance;
            if (index == 1) {
              firebaseAnalytics.setCurrentScreen(screenName: '/activities');
            } else if (index == 2) {
              firebaseAnalytics.setCurrentScreen(screenName: '/notification');
            } else if (index == 3) {
              firebaseAnalytics.setCurrentScreen(screenName: '/profile');
            } else {
              firebaseAnalytics.setCurrentScreen(screenName: '/home');
            }
          },
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.dashboard), label: 'Dashboard'),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.tickets), label: 'Activities'),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.bell), label: 'Alert'),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.person), label: 'Profile'),
          ],
        ),
        tabBuilder: (context, index) {
          // TODO
          switch (index) {
            case 0:
              return DashboardScreen();
            case 1:
              return ActivityMapScreen();
            case 2:
              return NotificationScreen();
            default:
              return ProfileScreen();
          }
        });
  }

  Future<void> _requestPushNotificationPermission() async {
    if (Platform.isIOS) {
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
          FlutterLocalNotificationsPlugin();
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );
    } else {
      FirebaseMessaging messaging = FirebaseMessaging.instance;

      NotificationSettings settings = await messaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );

      if (settings.authorizationStatus == AuthorizationStatus.authorized) {
        print('User granted permission');
      } else if (settings.authorizationStatus ==
          AuthorizationStatus.provisional) {
        print('User granted provisional permission');
      } else {
        print('User declined or has not accepted permission');
      }
    }
  }

  Future<void> _initUniLinks() async {
    try {
      final initialUri = await getInitialUri();
      if (initialUri != null) {
        _processReferral(initialUri);
      }

      _streamSubscription = uriLinkStream.listen((Uri? uri) {
        if (uri != null) _processReferral(uri);
      }, onError: (err) {
        Toast.show(context, 'danger', 'Failed to execute link.');
      });
    } on FormatException {
      Toast.show(context, 'danger', 'Failed to execute link.');
    } on PlatformException {
      Toast.show(context, 'danger', 'Failed to execute link.');
    }
  }

  Future<void> _processReferral(Uri uri) async {
    // TODO: Process referral link
  }
}
