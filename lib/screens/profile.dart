import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/screens/about.dart';
import 'package:jom/screens/bud.dart';
import 'package:jom/screens/friend.dart';
import 'package:jom/services/auth_service.dart';
import 'package:jom/services/database.dart';

class ProfileScreen extends StatefulWidget {
  dynamic user;
  late String userEmail;
  late String userPhoneNumber;
  ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int totalBudsAvatar = 7;
  int totalBuds = 0;
  String firstName = '';
  String uid = '';
  String mtoken = "";
  int friendBud = 0;
  String friendUrl = "";
  String? url;
  int bud = 1;
  String budUrl = "assets/images/bud1.png";
  List<dynamic> friends = [];
  PlatformFile? pickedFile;
  UploadTask? uploadTask;
  int tabIndex = 0;
  final _auth = FirebaseAuth.instance;
  @override
  void initState() {
    final User user = _auth.currentUser!;
    uid = user.displayName.toString();
    print(uid);
    _refreshContent();
    super.initState();
  }

  Future uploadFile() async {
    final path = 'images/userDP/$uid';
    final file = File(pickedFile!.path!);
    final ref = FirebaseStorage.instance.ref().child(path);
    uploadTask = ref.putFile(file);
    final snapshot = await uploadTask!.whenComplete(() {});
    final urlDownload = await snapshot.ref.getDownloadURL();
  }

  showUpload(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        uploadFile();
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Confirm Image",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  showBudPicker(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        DatabaseService().addUserBud(bud, uid);
        Navigator.pop(context);
        _refreshContent();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Select Bud",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      content: Container(
        height: 200,
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            physics: const AlwaysScrollableScrollPhysics(),
            itemCount: totalBudsAvatar,
            itemBuilder: (context, index) {
              int num = index + 1;
              return GestureDetector(
                  onTap: () {
                    setState(() {
                      bud = num;
                    });
                    print(bud);
                  },
                  child: Image.asset(
                    'assets/images/bud$num.png',
                    height: 35,
                  ));
            }),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  Future selectFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.image);
    setState(() {
      pickedFile = result!.files.first;
      if (pickedFile != null) {
        showUpload(context);
      }
    });
  }

  void selectBud() {
    showBudPicker(context);
  }

  void getUser() {
    FirebaseFirestore.instance.collection("user").doc(uid).get().then((value) {
      setState(() {
        firstName = value.get("name");
        bud = value.get("bud");
        budUrl = 'assets/images/bud$bud.png';
      });
    });
  }

  void getFriends() {
    FirebaseFirestore.instance.collection("user").doc(uid).get().then((value) {
      setState(() {
        friends = value.get("friends");

        totalBuds = friends.length;
      });
    });
  }

  Future<void> _refreshContent() async {
    getUser();
    getFriends();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: RefreshIndicator(
      color: Colors.black,
      onRefresh: _refreshContent,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(CupertinoPageRoute(builder: (context) {
                            return BudFormScreen();
                          }));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: Image.asset(
                            (budUrl != "") ? budUrl : 'assets/images/logo3.png',
                            height: 75,
                          ),
                        ),
                      ),
                      Text(
                        firstName,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        friends.length.toString() + " Buds",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 17,
                        ),
                      ),
                    ],
                  )),
              Column(
                children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 30),
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      tabIndex = 0;
                                    });
                                    Navigator.of(context).push(
                                        CupertinoPageRoute(builder: (context) {
                                      return AboutScreen();
                                    }));
                                  },
                                  child: Text(
                                    "About",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 17,
                                    ),
                                  )),
                              GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      tabIndex = 1;
                                    });
                                    Navigator.of(context).push(
                                        CupertinoPageRoute(builder: (context) {
                                      return FriendScreen();
                                    }));
                                  },
                                  child: Text(
                                    "Buds",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 17,
                                    ),
                                  )),
                              GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      tabIndex = 2;
                                    });
                                  },
                                  child: Text(
                                    "Settings",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 17,
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      )),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child:
                        Divider(color: Color.fromARGB(71, 0, 0, 0), height: 3),
                  ),
                  if (tabIndex == 0)
                    if (tabIndex == 1)
                      if (friends.isEmpty)
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/bud2.png',
                                height: 45,
                              ),
                              Text("You dont have friends yet add some"),
                              GestureDetector(
                                child: Icon(
                                  CupertinoIcons.add_circled,
                                  size: 25,
                                ),
                                onTap: () {
                                  Navigator.pushNamed(context, '/friend-form');
                                },
                              )
                            ],
                          ),
                        ),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Container(
                      decoration: BoxDecoration(
                        color: CupertinoTheme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 150,
                      height: 36,
                      child: SizedBox(
                        child: CupertinoButton(
                          disabledColor: CupertinoColors.inactiveGray,
                          padding: EdgeInsets.zero,
                          onPressed: () async {
                            await AuthenticationService.signOutGoogle(
                                context: context);
                            Navigator.pushReplacementNamed(context, '/login');
                          },
                          child: const Text('Logout',
                              style: TextStyle(color: CupertinoColors.white)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ])),
      ),
    ));
  }
}
