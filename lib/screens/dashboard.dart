import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:jom/adapters/activity.dart';
import 'package:jom/adapters/group.dart';
import 'package:jom/models/group.dart';
import 'package:jom/screens/activityprofile.dart';
import 'package:jom/screens/bud.dart';
import 'package:jom/screens/groupprofile.dart';
import 'package:jom/screens/friendprofile.dart';
import 'package:jom/screens/groupadd.dart';
import 'package:jom/services/database.dart';
import 'package:http/http.dart' as http;
import 'package:jom/utils/toast.dart';
import 'package:jom/widgets/search_bar.dart';

import 'package:permission_handler/permission_handler.dart';

import '../widgets/activity_bar.dart';
import '../widgets/forms/text_input.dart';
import 'friend.dart';

class DashboardScreen extends StatefulWidget {
  String? uid;
  DashboardScreen({Key? key, this.uid}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  String firstName = '';
  String uid = '';
  String mtoken = "";
  List<dynamic> friends = [];
  String id = "";
  List<int> friendIdList = [];
  String friendUrl = "";
  String friendToken = "";
  String friendId = "";
  Future<List<dynamic>?>? _futureGroups;
  late List<Group> groups = [];
  List<dynamic> members = [];
  List<dynamic> activities = [];
  final _nameController = TextEditingController();
  final _timeController = TextEditingController();
  final _locationController = TextEditingController();
  String latitude = "";
  String longitude = "";
  String url = "";
  int bud = 1;
  String budUrl = "";
  String googleName = "";
  List<int> friendBuds = [];
  List<int> reverseBuds = [];
  int friendBud = 0;
  int isAccept = 0;
  bool permissionLocation = false;
  bool permissionStorage = false;
  final _auth = FirebaseAuth.instance;
  late AndroidNotificationChannel channel;
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  @override
  void initState() {
    getUser();
    loadFCM();

    listenFCM();
    getToken();
    super.initState();
  }

  Future<void> _refreshContent() async {
    getUser();
    members.clear();
    friends.clear();
    groups.clear();

    friendUrl = "";
    requestPermission();
    _checkPermission();
    loadFCM();

    listenFCM();
    getToken();
    _futureGroups = getGroups();
  }

  void getUser() {
    final User user = _auth.currentUser!;
    googleName = user.displayName!;

    print(uid);
    print(googleName);
    FirebaseFirestore.instance
        .collection("user")
        .doc(googleName)
        .get()
        .then((value) {
      if (value.exists) {
        setState(() {
          firstName = value.get("name");
          bud = value.get("bud");
          budUrl = 'assets/images/bud$bud.png';
          print(firstName);
          if (value.get("friends") != null) {
            getFriends();
          }
          if (value.get("groups") != null) {
            getGroups();
          }
        });
      }
    });
  }

  Future<void> getFriends() async {
    await FirebaseFirestore.instance
        .collection("user")
        .doc(googleName)
        .get()
        .then((value) {
      setState(() {
        friends = value.get("friends");

        print(friends.length);
      });
    });
    for (int i = 0; i < friends.length; i++) {
      await FirebaseFirestore.instance
          .collection("user")
          .doc(friends[i]['name'])
          .get()
          .then((value) {
        setState(() {
          friendBuds.add(value.get("bud"));

          print(friendBuds.length);
        });
      });
    }
  }

  Future<List<dynamic>?>? getGroups() async {
    FirebaseFirestore.instance
        .collection("user")
        .doc(firstName)
        .get()
        .then((value) {
      var groupsList = value.get("groups");
      List<Group> _groupinfo =
          groupsList.map<Group>((json) => Group.fromJson(json)).toList();

      setState(() {
        groups = _groupinfo;
      });
      for (int i = 0; i < groups.length; i++) {
        FirebaseFirestore.instance
            .collection("group")
            .doc(groups[i].name)
            .get()
            .then((value) {
          setState(() {
            var memberList = value.get("members");
            List<Group> _memberinfo =
                memberList.map<Group>((json) => Group.fromJson(json)).toList();
            members = _memberinfo;

            print(members.length);
          });
        });
      }
      print(groups[0].name);
    });
    return groups;
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: RefreshIndicator(
      color: Colors.black,
      onRefresh: _refreshContent,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 10,
                      ),
                      child: Text(
                        "Hey Bud!",
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0),
                          fontSize: 28,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: Container(
                    child: ActivityBar(
                        onSubmitted: (value) => {
                              _nameController.text = value,
                              showQuickActivity(context, value)
                            })),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/friend-form');
                },
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Friends",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    Icon(
                      CupertinoIcons.add_circled,
                      size: 25,
                    )
                  ],
                ),
              ),
              if (friends.isEmpty)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/bud1.png',
                        height: 45,
                      ),
                      Text("You dont have friends yet add some"),
                    ],
                  ),
                ),
              if (friends.isNotEmpty)
                Container(
                  height: 67,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).padding.bottom),
                      physics: const AlwaysScrollableScrollPhysics(),
                      itemCount: friends.length,
                      itemBuilder: (context, index) {
                        var buds =
                            (friendBuds.isNotEmpty) ? friendBuds[index] : 0;
                        var bud = 'assets/images/bud$buds.png';
                        return GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(CupertinoPageRoute(builder: (context) {
                              return FriendScreen();
                            }));
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4),
                                    child: Image.asset(
                                      bud,
                                      height: 35,
                                    ),
                                  ),
                                ),
                                Text(friends[index]['name'])
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Divider(
                  thickness: 2,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context)
                      .push(CupertinoPageRoute(builder: (context) {
                    return GroupsFormScreen();
                  }));
                },
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Groups",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    Icon(
                      CupertinoIcons.add_circled,
                      size: 25,
                    )
                  ],
                ),
              ),
              if (groups.isEmpty)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/bud2.png',
                        height: 45,
                      ),
                      Text("You dont have groups yet join some"),
                    ],
                  ),
                ),
              if (groups.isNotEmpty)
                Container(
                  height: 200,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: SizedBox(
                      child: FutureBuilder<List<dynamic>?>(
                          future: _futureGroups,
                          builder: (context, snapshot) {
                            return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              padding: EdgeInsets.only(
                                  bottom:
                                      MediaQuery.of(context).padding.bottom),
                              physics: const AlwaysScrollableScrollPhysics(),
                              itemCount: groups.length,
                              itemBuilder: (context, index) {
                                return Container(
                                    padding: const EdgeInsets.all(10),
                                    child: GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).push(
                                              CupertinoPageRoute(
                                                  builder: (context) {
                                            return GroupsProfileScreen(
                                              uid: uid,
                                              id: groups[index].name,
                                              firstName: firstName,
                                              groupsName: groups[index].name,
                                              groupsUrl: groups[index].url,
                                              friends: friends,
                                            );
                                          }));
                                        },
                                        child: GroupAdapter(
                                            groupsName: groups[index].name,
                                            groupsUrl: groups[index].url,
                                            index: index,
                                            buds: members.length)));
                              },
                            );
                          }),
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Divider(
                  thickness: 2,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Your Activities ",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 150,
                child: StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestore.instance
                      .collection("activities")
                      .where("admin_name", isEqualTo: googleName)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const Center(
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.black),
                        ),
                      );
                    } else {
                      final docs = snapshot.data!.docs;

                      return Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: SizedBox(
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            physics: const AlwaysScrollableScrollPhysics(),
                            itemCount: docs.length,
                            itemBuilder: (context, index) {
                              String gName = docs[index]["group_name"];
                              String aPic = 'assets/images/bud$index.png';
                              String date = docs[index]["date"];
                              int join = docs[index]["joined"];
                              return Container(
                                  padding: const EdgeInsets.all(5),
                                  child: GestureDetector(
                                      onTap: () async {
                                        print(firstName);

                                        await Navigator.of(context).push(
                                            CupertinoPageRoute(
                                                builder: (context) {
                                          return ActivityProfileScreen(
                                            id: docs[index].id,
                                            uid: uid,
                                            groupsName: docs[index]["name"],
                                            groupDesc: docs[index]["time"],
                                            groupsUrl: docs[index]["url"],
                                            firstName: firstName,
                                            activityName: docs[index]["name"],
                                            activityLocation: docs[index]
                                                ["location"],
                                            activityDate:
                                                (docs[index]["date"] != null)
                                                    ? docs[index]["date"]
                                                    : "N/A",
                                            activityTime: docs[index]["time"],
                                          );
                                        }));
                                      },
                                      child: ActivityAdapter2(
                                          activity: docs,
                                          uid: uid,
                                          index: index,
                                          bud: join,
                                          firstName: firstName,
                                          friends: friends)));
                            },
                          ),
                        ),
                      );
                    }
                  },
                ),
              ),
            ])),
      ),
    ));
  }

  showQuickActivity(BuildContext context, String value) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
        DatabaseService().createActivity(
          firstName,
          _nameController.text,
          _timeController.text,
          _timeController.text,
          _locationController.text,
          latitude.toString(),
          latitude.toString(),
          "https://firebasestorage.googleapis.com/v0/b/jom-firaz.appspot.com/o/images%2Factivity%2FnoImage.png?alt=media&token=bbc20c82-bc72-40e1-bea0-1028f3c67145",
          true,
          firstName,
        );
        Toast.show(context, 'success', "Activity Created");
        _refreshContent();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Quick Activity",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      content: Container(
          height: 200,
          child: Column(
            children: [
              TextInput(
                  prefixWidth: 85,
                  label: 'Name',
                  controller: _nameController,
                  maxLength: 255,
                  required: true),
              TextInput(
                  prefixWidth: 85,
                  label: 'Time',
                  controller: _timeController,
                  maxLength: 255,
                  required: true),
              TextInput(
                  prefixWidth: 85,
                  label: 'Location',
                  controller: _locationController,
                  maxLength: 255,
                  required: true),
            ],
          )),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  _checkPermission() async {
    var statusLocation = await Permission.locationWhenInUse.request();
    var statusStorage = await Permission.storage.request();
    var statusCamera = await Permission.camera.request();

    var permissionLocationStatus = await Permission.locationWhenInUse.status;
    var permissionCameraStatus = await Permission.camera.status;
    var permissionStorageStatus = await Permission.storage.status;

    if (permissionLocationStatus == PermissionStatus.granted) if (mounted) {
      setState(() {
        permissionLocation = true;
      });
    }
    if (permissionStorageStatus == PermissionStatus.granted) if (mounted) {
      setState(() {
        permissionStorage = true;
      });
    }

    //todo:check permission status , show alert dialog when not granted
    // if (!permissionLocation || !permissionCamera || !permissionStorage) {
    //   showAlertPermissionDialog(context);
    // }
  }

  void requestPermission() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }

  void getToken() async {
    await FirebaseMessaging.instance.getToken().then((token) {
      setState(() {
        mtoken = token!;
      });
    });
  }

  void listenFCM() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null && !kIsWeb) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(channel.id, channel.name,
                icon: 'ic_notification', subText: "Buds"),
          ),
        );
      }
    });
  }

  void loadFCM() async {
    if (!kIsWeb) {
      channel = const AndroidNotificationChannel(
        'high_importance_channel', // id
        'High Importance Notifications', // title
        importance: Importance.high,
        enableVibration: true,
      );

      flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

      /// Create an Android Notification Channel.
      ///
      /// We use this channel in the `AndroidManifest.xml` file to override the
      /// default FCM channel to enable heads up notifications.
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);

      /// Update the iOS foreground notification presentation options to allow
      /// heads up notifications.
      await FirebaseMessaging.instance
          .setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );
    }
  }

  void sendPushMessage(
      String token, String title, String friendName, String friendID) async {
    try {
      await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization':
              'key=AAAA8zCtx9o:APA91bFHHBjYGb7ajnaKhPCa9L4ar_fo7GB0WVDR0XF8ZH5bQQlXZt1isAsPAoGJDY85uXCZ6nLLkiMO04aqwszZJdA_7KoBQblmAp6WQpbEXKVXtDbN1b5uUTP6lDCPYf4T38YPaqM9',
        },
        body: jsonEncode(
          <String, dynamic>{
            'notification': <String, dynamic>{
              'title': title,
            },
            'priority': 'high',
            'data': <String, dynamic>{
              'click_action': 'FLUTTER_NOTIFICATION_CLICK',
              'id': '1',
              'status': 'done'
            },
            "to": token,
          },
        ),
      );
      DatabaseService().createNotification(
          uid, friendID, firstName, friendName, token, title);
    } catch (e) {
      print("error push notification");
    }
  }
}
