import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:jom/adapters/activity.dart';
import 'package:jom/config/main.dart';
import 'package:jom/screens/activityprofile.dart';
import 'package:jom/screens/location_search.dart';
import 'package:jom/services/notification.dart';
import 'package:time_picker_sheet/widget/sheet.dart';
import 'package:time_picker_sheet/widget/time_picker.dart';
import 'package:google_place/google_place.dart';
import '../services/database.dart';
import '../utils/toast.dart';
import '../widgets/activity_bar.dart';
import '../widgets/forms/text_input.dart';

class GroupsProfileScreen extends StatefulWidget {
  dynamic groups;
  String? uid;
  String? firstName;
  late String? groupsName;
  String? groupsUrl;
  String? id;
  String? groupDesc;
  late int? guest;
  dynamic friends;
  GroupsProfileScreen(
      {Key? key,
      this.groups,
      this.groupsName,
      this.guest,
      this.groupDesc,
      this.id,
      this.firstName,
      this.groupsUrl,
      this.uid,
      this.friends})
      : super(key: key);

  @override
  _GroupsProfileScreenState createState() => _GroupsProfileScreenState();
}

class _GroupsProfileScreenState extends State<GroupsProfileScreen> {
  String id = '';
  List<dynamic> members = [];
  List<dynamic> activities = [];
  final _nameController = TextEditingController();
  final _timeController = TextEditingController();
  final _locationController = TextEditingController();
  DateTime dateTimeSelected = DateTime.now();
  double latitude = 0.0;
  double longitude = 0.0;
  String locationText = "";
  String memberName = "";
  int totalMembers = 1;
  String time = "";
  String date = "";
  String activityName = "";
  int join = 1;
  String adminName = "";
  @override
  void initState() {
    setState(() {
      id = widget.id!;
    });
    print(id);
    getMembers();
    super.initState();
  }

  void getMembers() async {
    if (members.isEmpty) {
      FirebaseFirestore.instance
          .collection("group")
          .doc(id)
          .get()
          .then((value) {
        setState(() {
          members = value.get("members");
          totalMembers = members.length;
          activities = value.get("activities");
        });
        print(activities.length);
      });
      for (int i = 0; i < activities.length; i++) {
        FirebaseFirestore.instance
            .collection("activities")
            .doc(id)
            .get()
            .then((value) {
          setState(() {
            activityName = value.get("name");
            join = value.get("joined");

            adminName = value.get('admin_name');
            members = value.get("members");
          });
        });
      }
    }
  }

  showAddMember(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {},
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Add members",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      content: Container(
        height: 200,
        child: Container(
          height: 65,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom),
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: widget.friends.length,
              itemBuilder: (context, index) {
                int num = index + 2;
                var friendName = widget.friends[index]['name'];
                return (memberName != friendName)
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                _submitForm(context, friendName);
                                Navigator.pop(context);
                              },
                              child: Card(
                                elevation: 5,
                                child: Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Image.asset(
                                    'assets/images/bud$num.png',
                                    height: 45,
                                  ),
                                ),
                              ),
                            ),
                            Text(friendName)
                          ],
                        ),
                      )
                    : Container();
              }),
        ),
      ),
      actions: [
        cancelButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  void _openTimePickerSheet(BuildContext context, String value) async {
    final result = await TimePicker.show<DateTime?>(
      context: context,
      sheet: TimePickerSheet(
        wheelNumberSelectedStyle: TextStyle(),
        hourTitleStyle: TextStyle(),
        minuteTitleStyle: TextStyle(),
        sheetCloseIconColor: Colors.black,
        saveButtonColor: Colors.black,
        sheetTitle: 'Select meeting schedule',
        minuteTitle: 'Minute',
        hourTitle: 'Hour',
        saveButtonText: 'Save',
      ),
    );

    if (result != null) {
      setState(() {
        dateTimeSelected = result;
        Navigator.pop(context);
        showQuickActivity(context, value);
      });
    }
  }

  Future<void> _submitActivityForm(BuildContext context) async {
    String name = _nameController.text;
    String location = locationText;
    String firstName = widget.firstName!;
    print(firstName);
    String uid = widget.uid!;
    List<String> errors = [];

    if (errors.isEmpty) {
      String? messagingToken =
          await NotificationService().getFirebaseMessagingToken();

      DatabaseService().addActivity(
          firstName,
          name,
          date,
          time,
          location,
          latitude.toString(),
          longitude.toString(),
          "https://firebasestorage.googleapis.com/v0/b/jom-firaz.appspot.com/o/images%2Factivity%2FnoImage.png?alt=media&token=bbc20c82-bc72-40e1-bea0-1028f3c67145",
          true,
          widget.groupsName!);
      DatabaseService().createActivity(
          firstName,
          name,
          date,
          time,
          location,
          latitude.toString(),
          longitude.toString(),
          "https://firebasestorage.googleapis.com/v0/b/jom-firaz.appspot.com/o/images%2Factivity%2FnoImage.png?alt=media&token=bbc20c82-bc72-40e1-bea0-1028f3c67145",
          true,
          widget.groupsName!);
      Toast.show(context, 'success', "Activity Created");
      Navigator.pop(context);
    }

    if (errors.isNotEmpty) {
      Toast.show(context, 'danger', errors[0]);
    }
  }

  showQuickActivity(BuildContext context, String value) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
        _submitActivityForm(
          context,
        );
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Create Activity",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      content: Container(
          height: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TextInput(
                  prefixWidth: 85,
                  label: 'Name',
                  controller: _nameController,
                  maxLength: 255,
                  required: true),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () async {
                    await DatePicker.showDateTimePicker(context,
                        showTitleActions: true, onChanged: (dateTemp) {
                      print('change $dateTemp');
                    }, onConfirm: (dateTemp) {
                      setState(() {
                        date = dateTemp.toString();
                        var arr2 = date.split(' ');
                        date = arr2[0];

                        time = arr2[1];
                        var arr3 = time.split('.');
                        time = arr3[0];
                      });

                      print('confirm $date');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                    Navigator.pop(context);
                    showQuickActivity(context, value);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text('Date',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Icon(CupertinoIcons.calendar),
                      SizedBox(
                        width: 55,
                      ),
                      Text(
                        date,
                        style: TextStyle(fontSize: 14),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Time', style: TextStyle(fontWeight: FontWeight.bold)),
                    Icon(CupertinoIcons.clock),
                    Text(
                      time,
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
              ),
              Divider(
                height: 1,
              ),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () async {
                    DetailsResult location = await Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (context) {
                      return LocationSearch(
                          locationController: _locationController);
                    }));
                    if (location != "") {
                      setState(() {
                        locationText = location.name!;
                        latitude = location.geometry!.location!.lat!;
                        longitude = location.geometry!.location!.lng!;
                      });

                      Navigator.pop(context);
                      showQuickActivity(context, value);
                    }
                    print(locationText);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text('Location',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Icon(CupertinoIcons.map),
                      SizedBox(
                        width: 15,
                      ),
                      Text(locationText),
                    ],
                  ),
                ),
              ),
            ],
          )),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Column(children: [
                Align(
                    alignment: Alignment.topCenter,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black,
                                border: Border.all(color: Colors.white),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromARGB(255, 255, 255, 255)
                                        .withOpacity(0.6),
                                    spreadRadius: 1,
                                    blurRadius: 1,
                                    offset: const Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Image.network(
                                widget.groupsUrl!,
                                fit: BoxFit.cover,
                              ),
                              height: 200,
                              width: double.infinity,
                            ),
                          ),
                        ),
                      ],
                    )),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    boxShadow: [
                      BoxShadow(
                        color:
                            Color.fromARGB(255, 255, 255, 255).withOpacity(0.6),
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        widget.groupsName!,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 25,
                        ),
                      ),
                    ),
                    Divider(color: CupertinoColors.separator, height: 1),
                    Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                    "activities",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(15),
                              child: Container(
                                  child: ActivityBar(
                                      onSubmitted: (value) => {
                                            _nameController.text = value,
                                            showQuickActivity(context, value)
                                          })),
                            ),
                            Container(
                              height: 175,
                              child: StreamBuilder<QuerySnapshot>(
                                stream: FirebaseFirestore.instance
                                    .collection("activities")
                                    .snapshots(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return const Center(
                                      child: CircularProgressIndicator(
                                        valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                                Colors.black),
                                      ),
                                    );
                                  } else {
                                    final docs = snapshot.data!.docs;

                                    return Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 10.0),
                                      child: SizedBox(
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          physics:
                                              const AlwaysScrollableScrollPhysics(),
                                          itemCount: activities.length,
                                          itemBuilder: (context, index) {
                                            String gName = widget.groupsName!;
                                            print(gName);
                                            // getMembersUrl(gId);

                                            return Container(
                                                padding:
                                                    const EdgeInsets.all(15),
                                                child: GestureDetector(
                                                    onTap: () {
                                                      Navigator.of(context).push(
                                                          CupertinoPageRoute(
                                                              builder:
                                                                  (context) {
                                                        return ActivityProfileScreen(
                                                          id: activities[index]
                                                              ["name"]!,
                                                          activityName:
                                                              activities[index]
                                                                  ["name"]!,
                                                          activityLocation:
                                                              activities[index]
                                                                  ["location"],
                                                          activityTime:
                                                              activities[index]
                                                                  ["time"],
                                                          activityDate:
                                                              activities[index]
                                                                  ["date"],
                                                          groupsName:
                                                              activities[index][
                                                                  "group_name"],
                                                          firstName:
                                                              widget.firstName,
                                                        );
                                                      }));
                                                    },
                                                    child: ActivityAdapter2(
                                                        activity: docs,
                                                        uid: widget.firstName!,
                                                        index: index,
                                                        bud: join,
                                                        firstName:
                                                            widget.firstName,
                                                        friends:
                                                            widget.friends)));
                                          },
                                        ),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ],
                        )),
                    Divider(color: CupertinoColors.separator, height: 1),
                    Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        "members",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                        height: 175,
                                        width: 100,
                                        child: Card(
                                          elevation: 5,
                                          child: StreamBuilder<QuerySnapshot>(
                                              stream: FirebaseFirestore.instance
                                                  .collection("groups")
                                                  .snapshots(),
                                              builder: (context, snapshot) {
                                                if (!snapshot.hasData) {
                                                  return const Center(
                                                    child:
                                                        CircularProgressIndicator(
                                                      valueColor:
                                                          AlwaysStoppedAnimation<
                                                                  Color>(
                                                              Colors.black),
                                                    ),
                                                  );
                                                } else {
                                                  final docs =
                                                      snapshot.data!.docs;

                                                  return Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              5),
                                                      child: ListView.builder(
                                                          itemCount:
                                                              members.length,
                                                          itemBuilder:
                                                              (context, index) {
                                                            int reversedIndex =
                                                                members.length -
                                                                    1 -
                                                                    index;
                                                            memberName = members[
                                                                    reversedIndex]
                                                                ["name"];
                                                            return Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(2),
                                                              child: Text(
                                                                (index + 1)
                                                                        .toString() +
                                                                    " " +
                                                                    memberName,
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .clip,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        15),
                                                              ),
                                                            );
                                                          }));
                                                }
                                              }),
                                        ))
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.all(5),
                                  child: CupertinoButton(
                                    onPressed: () {
                                      showAddMember(
                                        context,
                                      );
                                    },
                                    child: Column(
                                      children: [
                                        Icon(
                                          CupertinoIcons.add_circled,
                                          size: 40,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Text((totalMembers != 0)
                                        ? "total members: " +
                                            totalMembers.toString()
                                        : "N/A"),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Container(
                        decoration: BoxDecoration(
                          color: CupertinoTheme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(25),
                        ),
                        width: 150,
                        height: 36,
                        child: SizedBox(
                          child: CupertinoButton(
                            disabledColor: CupertinoColors.inactiveGray,
                            padding: EdgeInsets.zero,
                            onPressed: () async {
                              Navigator.pop(context);
                            },
                            child: const Text('Back',
                                style: TextStyle(color: CupertinoColors.white)),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          border: Border.all(color: Colors.black),
                          boxShadow: [
                            BoxShadow(
                              color: CupertinoColors.lightBackgroundGray,
                              spreadRadius: 1,
                              blurRadius: 1,
                              offset: const Offset(
                                  0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        width: 150,
                        height: 36,
                        child: SizedBox(
                          child: CupertinoButton(
                            disabledColor: CupertinoColors.inactiveGray,
                            padding: EdgeInsets.zero,
                            onPressed: () async {
                              _deleteGroup();
                            },
                            child: const Text('Delete group',
                                style: TextStyle(color: CupertinoColors.black)),
                          ),
                        ),
                      ),
                    ),
                  ]),
                )
              ]),
            )));
  }

  Future<void> _submitForm(BuildContext context, String friendName) async {
    String gname = widget.groupsName!;
    String token = "";
    List<String> errors = [];
    print(friendName);
    if (errors.isEmpty) {
      String? messagingToken =
          await NotificationService().getFirebaseMessagingToken();
      FirebaseFirestore.instance
          .collection("user")
          .where("name", isEqualTo: friendName)
          .get()
          .then((querySnapshot) async {
        if (!querySnapshot.docs.isEmpty) {
          setState(() {
            friendName = querySnapshot.docs[0].get("name");
            token = querySnapshot.docs[0].get("token");
          });
          print(friendName);
          await DatabaseService().memberNotification(
              widget.firstName!, friendName, token, "Member Request", gname);
          Toast.show(context, 'success', "Bud added");
        } else {
          Toast.show(context, 'danger', "Bud not found");
          print("Document Doesn't Exist");
        }
      });
    }

    if (errors.isNotEmpty) {
      Toast.show(context, 'danger', errors[0]);
    }
  }

  Future<void> _deleteGroup() async {
    await DatabaseService().deleteGroup(widget.uid!, widget.groupsName!,
        widget.groupDesc!, "", widget.groupsUrl!);
    await DatabaseService().deletegroup(widget.id!);
    Navigator.pop(context);
    Toast.show(context, "succeess", "Group Deleted");
  }
}
