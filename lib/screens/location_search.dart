import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_place/google_place.dart';
import 'package:jom/config/main.dart';
import 'package:search_map_location/utils/google_search/latlng.dart';

class LocationSearch extends StatefulWidget {
  TextEditingController locationController;
  LocationSearch({Key? key, required this.locationController})
      : super(key: key);

  @override
  State<LocationSearch> createState() => _LocationSearchState();
}

class _LocationSearchState extends State<LocationSearch> {
  late GooglePlace googlePlace;
  List<AutocompletePrediction> predictions = [];
  Timer? _debounce;
  DetailsResult? locationResult;
  final _locationController = TextEditingController();

  LatLon? latlong;
  String latitude = "";
  String longitude = "";
  @override
  void initState() {
    super.initState();
    String apiKey = MainConfig.googleMapAPi;
    googlePlace = GooglePlace(apiKey);
  }

  getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      latitude = position.latitude.toString();
      longitude = position.longitude.toString();
    });
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete
        .get(value, region: "my", location: latlong);
    if (result != null && result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions!;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    padding: const EdgeInsets.all(15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(3),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context, "");
                            },
                            child: const Icon(
                              Icons.arrow_back_ios_new,
                              size: 25,
                            ),
                          ),
                        ),
                        const Spacer(),
                        Text(
                          "Search location",
                          style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 25,
                          ),
                        ),
                        const Spacer(),
                      ],
                    ),
                  ),
                ),
                Card(
                  child: TextField(
                    controller: _locationController,
                    autofocus: false,
                    style: TextStyle(fontSize: 24),
                    decoration: InputDecoration(
                        hintText: 'Enter location',
                        hintStyle: const TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 20),
                        border: InputBorder.none),
                    onChanged: (value) {
                      if (_debounce?.isActive ?? false) _debounce!.cancel();
                      _debounce = Timer(const Duration(milliseconds: 1000), () {
                        if (value.isNotEmpty) {
                          //places api
                          autoCompleteSearch(value);
                        } else {
                          //clear out the results
                        }
                      });
                    },
                  ),
                ),
                Card(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: predictions.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Colors.black,
                            child: Icon(
                              CupertinoIcons.map_pin,
                              color: Colors.white,
                            ),
                          ),
                          title: Text(
                            predictions[index].description.toString(),
                          ),
                          onTap: () async {
                            final placeId = predictions[index].placeId!;
                            final details =
                                await googlePlace.details.get(placeId);
                            if (details != null &&
                                details.result != null &&
                                mounted) {
                              setState(() {
                                locationResult = details.result;

                                predictions = [];
                              });
                              Navigator.pop(context, locationResult);
                            }
                          },
                        );
                      }),
                )
              ],
            ),
          )),
    ));
  }
}
