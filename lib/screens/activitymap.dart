import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:jom/adapters/activity_maps.dart';
import 'package:jom/adapters/p_activity.dart';
import 'package:jom/screens/activitylist.dart';
import 'package:jom/screens/friendprofile.dart';

import '../widgets/search_bar.dart';
import 'activityprofile.dart';

class ActivityMapScreen extends StatefulWidget {
  const ActivityMapScreen({Key? key}) : super(key: key);

  @override
  _ActivityMapScreenState createState() => _ActivityMapScreenState();
}

class _ActivityMapScreenState extends State<ActivityMapScreen> {
  final _auth = FirebaseAuth.instance;
  String firstName = '';
  String uid = '';
  String mtoken = "";
  List<dynamic> groups = [];
  List<dynamic> members = [];
  List<dynamic> activities = [];
  List<String> name = [];
  List<String> latitudes = [];
  List<String> longitudes = [];
  double latitude = 0;
  double longitude = 0;

  @override
  void initState() {
    getUser();

    super.initState();
  }

  void getUser() {
    final User user = _auth.currentUser!;
    uid = user.uid.toString();
    FirebaseFirestore.instance.collection("user").doc(uid).get().then((value) {
      if (value.exists) {
        setState(() {
          firstName = value.get("name");
          groups = value.get("groups");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: Column(children: [
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, '/groups-form');
          },
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "What your buds are doing",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height - 150,
            child: StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance
                  .collection("activities")
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                    ),
                  );
                } else {
                  final docs = snapshot.data!.docs;

                  return Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: SizedBox(
                        child: GoogleMapsWidget(
                      activity: docs,
                    )),
                  );
                }
              },
            ),
          ),
        ),
      ]),
    ));
  }
}
