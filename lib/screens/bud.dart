// ignore_for_file: prefer_const_constructors_in_immutables

import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:jom/services/database.dart';
import 'package:time_picker_sheet/widget/sheet.dart';
import 'package:time_picker_sheet/widget/time_picker.dart';

import '../services/notification.dart';
import '../utils/toast.dart';
import '../widgets/forms/text_input.dart';

class BudFormScreen extends StatefulWidget {
  BudFormScreen({Key? key}) : super(key: key);
  @override
  _BudFormScreenState createState() => _BudFormScreenState();
}

class _BudFormScreenState extends State<BudFormScreen> {
  ControlsDetails? details2;
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;
  bool onComplete = false;
  int swipe = 0;
  int totalBuds = 7;
  int bud = 1;
  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneNoController = TextEditingController();
  final _passwordController = TextEditingController();
  String url = "";
  PlatformFile? pickedFile;
  UploadTask? uploadTask;
  bool isPublic = false;
  String firstName = "";
  DateTime dateTimeSelected = DateTime.now();
  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNoController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CupertinoPageScaffold(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
        ),
        child: SingleChildScrollView(
          child: Column(children: [
            Center(
              child: Container(
                padding: const EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(3),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.arrow_back_ios_new,
                          size: 25,
                        ),
                      ),
                    ),
                    const Spacer(),
                    Text(
                      "Make Your bud".tr,
                      style: const TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 20,
                      ),
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            ),
            _simkaSteps(),
          ]),
        ),
      ),
    ));
  }

  _simkaSteps() {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(
        height: MediaQuery.of(context).size.height * 80 / 100,
        width: 400,
      ),
      child: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: Colors.black)),
        child: Container(
          padding: const EdgeInsets.only(top: 5),
          child: Stepper(
            type: stepperType,
            physics: const ClampingScrollPhysics(),
            currentStep: _currentStep,
            onStepTapped: (step) => tapped(step),
            onStepContinue: continued,
            onStepCancel: cancel,
            controlsBuilder: (BuildContext context, ControlsDetails details) {
              return (onComplete)
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50.0),
                      child: SizedBox(
                        height: 50,
                        child: CupertinoButton(
                            borderRadius: BorderRadius.circular(15),
                            color: CupertinoTheme.of(context).primaryColor,
                            disabledColor: CupertinoColors.black,
                            onPressed: () {
                              complete(context);
                            },
                            child: Text(
                              'Proceed'.tr,
                              style: const TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w300,
                                  fontSize: 15),
                            )),
                      ),
                    )
                  : Container(
                      padding: const EdgeInsets.all(8.0),
                    );
            },
            steps: <Step>[
              Step(
                title: const Text(
                  '',
                ),
                content: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'Enter Username'.tr,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w300,
                            fontSize: 20,
                            color: Colors.black),
                      ),
                    ),
                    Card(
                      child: TextInput(
                        prefixWidth: 85,
                        label: '',
                        controller: _nameController,
                        maxLength: 255,
                        onEditingComplete: () {
                          _currentStep++;
                        },
                      ),
                    ),
                  ],
                ),
                isActive: _currentStep >= 2,
                state:
                    _currentStep >= 2 ? StepState.complete : StepState.indexed,
              ),
              Step(
                title: const Text(
                  '',
                ),
                content: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'Select your first bud'.tr,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w300,
                            fontSize: 20,
                            color: Colors.black),
                      ),
                    ),
                    Text(
                      'Slide for more ->'.tr,
                      style: const TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w300,
                          fontSize: 18,
                          color: Colors.black),
                    ),
                    Container(
                      height: 220,
                      color: Colors.white,
                      child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          physics: const AlwaysScrollableScrollPhysics(),
                          itemCount: totalBuds,
                          itemBuilder: (context, index) {
                            int num = index + 1;
                            return GestureDetector(
                                onTap: () {
                                  setState(() {
                                    bud = num;
                                    complete(context);
                                  });

                                  print(bud);
                                },
                                child: Image.asset(
                                  'assets/images/bud$num.png',
                                  height: 15,
                                ));
                          }),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        color: Colors.white,
                        padding: const EdgeInsets.all(15),
                        child: Text(
                          "Bud $bud",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                isActive: _currentStep >= 3,
                state:
                    _currentStep >= 3 ? StepState.complete : StepState.indexed,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void complete(BuildContext context) {
    _submitForm(context);
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    _currentStep < 3 ? setState(() => _currentStep += 1) : null;

    _currentStep >= 3 ? onComplete = true : null;
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }

  void _openTimePickerSheet(BuildContext context) async {
    final result = await TimePicker.show<DateTime?>(
      context: context,
      sheet: TimePickerSheet(
        wheelNumberSelectedStyle: TextStyle(),
        hourTitleStyle: TextStyle(),
        minuteTitleStyle: TextStyle(),
        sheetCloseIconColor: Colors.black,
        saveButtonColor: Colors.black,
        sheetTitle: 'Select meeting schedule',
        minuteTitle: 'Minute',
        hourTitle: 'Hour',
        saveButtonText: 'Save',
      ),
    );

    if (result != null) {
      setState(() {
        dateTimeSelected = result;
        _currentStep++;
      });
    }
  }

  showUpload(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () async {
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Confirm Image",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  Future selectFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.image);
    setState(() {
      pickedFile = result!.files.first;
      if (pickedFile != null) {
        showUpload(context);
      }
    });
  }

  void getUser(String uid) {
    FirebaseFirestore.instance.collection("user").doc(uid).get().then((value) {
      setState(() {
        firstName = value.get("name");
      });
    });
  }

  Future<dynamic> getData() async {}
  Future<void> _submitForm(BuildContext context) async {
    String name = _nameController.text;

    print(firstName);
    User uid = _auth.currentUser!;

    List<String> errors = [];

    if (errors.isEmpty) {
      String? messagingToken =
          await NotificationService().getFirebaseMessagingToken();
      await uid.updateDisplayName(name);
      await DatabaseService().registerUserData(
        name,
        messagingToken!,
        bud,
      );

      Toast.show(context, 'success', "Bud Updated");
      Navigator.pushReplacementNamed(context, '/home');
    }

    if (errors.isNotEmpty) {
      Toast.show(context, 'danger', errors[0]);
    }
  }
}
