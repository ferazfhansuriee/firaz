// ignore_for_file: prefer_const_constructors_in_immutables

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:jom/services/database.dart';

import '../services/notification.dart';
import '../utils/toast.dart';
import '../widgets/forms/text_input.dart';

class GroupsFormScreen extends StatefulWidget {
  GroupsFormScreen({
    Key? key,
  }) : super(key: key);
  @override
  _GroupsFormScreenState createState() => _GroupsFormScreenState();
}

class _GroupsFormScreenState extends State<GroupsFormScreen> {
  ControlsDetails? details2;
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;
  bool onComplete = false;
  int swipe = 0;
  final _auth = FirebaseAuth.instance;
  final _nameController = TextEditingController();
  final _descController = TextEditingController();
  final _memberController = TextEditingController();
  String url = "";
  PlatformFile? pickedFile;
  UploadTask? uploadTask;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CupertinoPageScaffold(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
        ),
        child: SingleChildScrollView(
          child: Column(children: [
            Center(
              child: Container(
                padding: const EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(3),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.arrow_back_ios_new,
                          size: 25,
                        ),
                      ),
                    ),
                    const Spacer(),
                    Text(
                      "Create Group".tr,
                      style: const TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 20,
                      ),
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            ),
            _simkaSteps(),
          ]),
        ),
      ),
    ));
  }

  _simkaSteps() {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(
        height: MediaQuery.of(context).size.height * 80 / 100,
        width: 400,
      ),
      child: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: Colors.black)),
        child: Container(
          padding: const EdgeInsets.only(top: 5),
          child: Stepper(
            type: stepperType,
            physics: const ClampingScrollPhysics(),
            currentStep: _currentStep,
            onStepTapped: (step) => tapped(step),
            onStepContinue: continued,
            onStepCancel: cancel,
            controlsBuilder: (BuildContext context, ControlsDetails details) {
              return (onComplete)
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50.0),
                      child: SizedBox(
                        height: 50,
                        child: CupertinoButton(
                            borderRadius: BorderRadius.circular(15),
                            color: CupertinoTheme.of(context).primaryColor,
                            disabledColor: CupertinoColors.black,
                            onPressed: () {
                              complete(context);
                            },
                            child: Text(
                              'Proceed'.tr,
                              style: const TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w300,
                                  fontSize: 15),
                            )),
                      ),
                    )
                  : Container(
                      padding: const EdgeInsets.all(8.0),
                    );
            },
            steps: <Step>[
              Step(
                title: const Text(
                  '',
                ),
                content: Column(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/bud6.png',
                      height: MediaQuery.of(context).size.height * 30 / 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        "new buds ".tr,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w300,
                            fontSize: 20,
                            color: Colors.black),
                      ),
                    ),
                    Text(
                      "what do you call these buds".tr,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 20,
                          fontWeight: FontWeight.w300,
                          color: Colors.black),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: TextInput(
                          onEditingComplete: () {
                            setState(() {
                              _currentStep++;
                            });
                          },
                          prefixWidth: 85,
                          label: '',
                          controller: _nameController,
                          maxLength: 255,
                          required: true),
                    ),
                  ],
                ),
                isActive: _currentStep >= 0,
                state:
                    _currentStep >= 0 ? StepState.complete : StepState.indexed,
              ),
              Step(
                title: const Text(''),
                content: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'your group needs a pic'.tr,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w300,
                            fontSize: 20,
                            color: Colors.black),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: selectFile,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(color: Colors.white),
                                    boxShadow: [
                                      BoxShadow(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255)
                                                .withOpacity(0.6),
                                        spreadRadius: 1,
                                        blurRadius: 1,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  height: 150,
                                  width: 150,
                                  child: (pickedFile != null)
                                      ? Image.file(
                                          File(pickedFile!.path!),
                                          width: double.infinity,
                                          fit: BoxFit.cover,
                                        )
                                      : Container()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                isActive: _currentStep >= 1,
                state:
                    _currentStep >= 1 ? StepState.complete : StepState.indexed,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void complete(BuildContext context) {
    _submitForm(context);
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    _currentStep < 4 ? setState(() => _currentStep += 1) : null;

    _currentStep >= 3 ? onComplete = true : null;
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }

  Future uploadFile() async {
    final path = 'images/budgroup/' + _nameController.text;
    final file = File(pickedFile!.path!);
    final ref = FirebaseStorage.instance.ref().child(path);
    uploadTask = ref.putFile(file);
    final snapshot = await uploadTask!.whenComplete(() {});
    final urlDownload = await snapshot.ref.getDownloadURL();
    url = urlDownload;
    _submitForm(context);
    print(url);
  }

  showUpload(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        uploadFile();

        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Confirm Image",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  Future selectFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.image);
    setState(() {
      pickedFile = result!.files.first;
      if (pickedFile != null) {
        showUpload(context);
      }
    });
  }

  Future<dynamic> getData() async {}
  Future<void> _submitForm(BuildContext context) async {
    String name = _nameController.text;
    String firstName = _auth.currentUser!.displayName!;
    String desc = _descController.text;
    String members = _memberController.text;
    String uid = _auth.currentUser!.uid;

    List<String> errors = [];

    if (errors.isEmpty) {
      String? messagingToken =
          await NotificationService().getFirebaseMessagingToken();

      DatabaseService().createGroup(
        name,
        firstName,
        url,
      );
      DatabaseService().addGroup(firstName, name, url);
      Navigator.pushReplacementNamed(context, '/home');
    }

    if (errors.isNotEmpty) {
      Toast.show(context, 'danger', errors[0]);
    }
  }
}
