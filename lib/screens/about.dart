import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/screens/bud.dart';
import 'package:jom/screens/friend.dart';
import 'package:jom/services/auth_service.dart';
import 'package:jom/services/database.dart';

class AboutScreen extends StatefulWidget {
  dynamic user;
  late String userEmail;
  late String userPhoneNumber;
  AboutScreen({Key? key}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  int totalBudsAvatar = 7;
  int totalBuds = 0;
  String firstName = '';
  String uid = '';
  String mtoken = "";
  int friendBud = 0;
  String friendUrl = "";
  String? url;
  int bud = 1;
  String budUrl = "assets/images/bud1.png";
  List<dynamic> friends = [];
  PlatformFile? pickedFile;
  UploadTask? uploadTask;
  int tabIndex = 0;
  final _auth = FirebaseAuth.instance;
  @override
  void initState() {
    final User user = _auth.currentUser!;
    uid = user.uid.toString();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(children: [
            Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Image.asset(
                          'assets/images/bud1.png',
                          height: 75,
                        ),
                      ),
                    ),
                    Text(
                      'About Buds',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      "feraz made buds,he wants money",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          border: Border.all(color: Colors.black),
                          boxShadow: [
                            BoxShadow(
                              color: CupertinoColors.lightBackgroundGray,
                              spreadRadius: 1,
                              blurRadius: 1,
                              offset: const Offset(
                                  0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        width: 150,
                        height: 36,
                        child: SizedBox(
                          child: CupertinoButton(
                            disabledColor: CupertinoColors.inactiveGray,
                            padding: EdgeInsets.zero,
                            onPressed: () async {
                              Navigator.pop(context);
                            },
                            child: const Text('Back',
                                style: TextStyle(color: CupertinoColors.black)),
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ])),
    ));
  }
}
