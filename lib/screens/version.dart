import 'package:flutter/cupertino.dart';

class VersionScreen extends StatelessWidget {
  const VersionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(CupertinoIcons.exclamationmark_triangle, size: 90),
              Container(
                padding: const EdgeInsets.only(top: 30, bottom: 20),
                child: const Text('App Update Required', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
              ),
              const Text('A new version of the app is available for download. Please update your app to continue using it.',textAlign: TextAlign.center),
            ],
          ),
        ),
      ),
    );
  }
}
