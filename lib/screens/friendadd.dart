import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/services/auth_service.dart';
import 'package:jom/services/database.dart';
import 'package:jom/services/notification.dart';
import '../global.dart' as global;
import '../models/user.dart';

import '../utils/progress_dialog.dart';
import '../utils/toast.dart';
import '../widgets/forms/phone_no_input.dart';
import '../widgets/forms/text_input.dart';

class FriendFormScreen extends StatefulWidget {
  const FriendFormScreen({Key? key}) : super(key: key);

  @override
  _FriendFormScreenState createState() => _FriendFormScreenState();
}

class _FriendFormScreenState extends State<FriendFormScreen> {
  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneNoController = TextEditingController();
  String firstName = "";

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: CupertinoPageScaffold(
        navigationBar: const CupertinoNavigationBar(
          middle: Text('Friend Form'),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).padding.top + 44),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Column(
                      children: [
                        TextInput(
                            prefixWidth: 85,
                            label: 'Name',
                            controller: _nameController,
                            maxLength: 255,
                            required: true),
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    decoration: const BoxDecoration(
                      color: CupertinoColors.white,
                      border: Border(
                        bottom: BorderSide(color: CupertinoColors.systemGrey5),
                      ),
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      height: 36,
                      child: CupertinoButton(
                        color: CupertinoTheme.of(context).primaryColor,
                        disabledColor: CupertinoColors.inactiveGray,
                        padding: EdgeInsets.zero,
                        onPressed: () async {
                          _submitForm(context);
                        },
                        child: const Text('Add friend',
                            style: TextStyle(color: CupertinoColors.white)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _submitForm(BuildContext context) async {
    String reciever_name = _nameController.text;
    String sender_name = _auth.currentUser!.displayName!;
    String uid = _auth.currentUser!.uid;
    String id = "";

    String token = "";
    print(sender_name);
    List<String> errors = [];

    if (errors.isEmpty) {
      String? messagingToken =
          await NotificationService().getFirebaseMessagingToken();

      final GlobalKey progressDialogKey = GlobalKey<State>();

      FirebaseFirestore.instance
          .collection("user")
          .where("name", isEqualTo: _nameController.text)
          .get()
          .then((querySnapshot) async {
        if (!querySnapshot.docs.isEmpty &&
            _nameController.text != sender_name) {
          token = querySnapshot.docs[0].get("token");
          await DatabaseService().createNotification(
            uid,
            id,
            sender_name,
            reciever_name,
            token,
            "Friend Request",
          );
          Toast.show(context, 'success', "Friend Added");
          Navigator.pushReplacementNamed(context, '/home');
        } else {
          Toast.show(context, 'danger', "Friend not found");
          print("Document Doesn't Exist");
        }
      });
    }

    if (errors.isNotEmpty) {
      Toast.show(context, 'danger', errors[0]);
    }
  }
}
