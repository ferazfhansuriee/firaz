import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/screens/friendprofile.dart';

class FriendScreen extends StatefulWidget {
  const FriendScreen({Key? key}) : super(key: key);

  @override
  _FriendScreenState createState() => _FriendScreenState();
}

class _FriendScreenState extends State<FriendScreen> {
  String firstName = '';
  String uid = '';
  String mtoken = "";
  List<dynamic> friends = [];

  void initState() {
    final User user = FirebaseAuth.instance.currentUser!;
    uid = user.displayName.toString();
    getUser();
    super.initState();
  }

  void getUser() {
    FirebaseFirestore.instance.collection("user").doc(uid).get().then((value) {
      firstName = value.get("name");
      uid = value.get("uid");
      friends = value.get("friends");
      print(friends[0]);
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(children: [
            const Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                "Buds",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 28,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3),
              child: Divider(
                thickness: 2,
              ),
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Add buds",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 25,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 25),
              child: Card(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  width: 64,
                  height: 64,
                  child: SizedBox(
                    child: CupertinoButton(
                      disabledColor: CupertinoColors.inactiveGray,
                      padding: EdgeInsets.zero,
                      onPressed: () async {
                        Navigator.pushNamed(context, '/friend-form');
                      },
                      child: const Icon(
                        Icons.add,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 375,
              child: StreamBuilder<QuerySnapshot>(
                stream:
                    FirebaseFirestore.instance.collection("user").snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                      ),
                    );
                  } else {
                    final docs = snapshot.data!.docs;
                    print(docs.length);
                    return SizedBox(
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: friends.length,
                        itemBuilder: (context, index) {
                          final data = docs[index].data();
                          String name = friends[index]["name"];
                          int num = index + 1;
                          return Padding(
                              padding: const EdgeInsets.all(3),
                              child: GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(
                                        CupertinoPageRoute(builder: (context) {
                                      return FriendProfileScreen(
                                        friendName: name,
                                        friendUrl: 'assets/images/bud$num.png',
                                      );
                                    }));
                                  },
                                  child: Card(
                                    child: Container(
                                      height: 85,
                                      width: 50,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Image.asset(
                                                'assets/images/bud$num.png',
                                                height: 45,
                                              )),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              child: Center(
                                                child: Text(
                                                  name,
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: Colors.black),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: GestureDetector(
                                              onTap: () async {
                                                String titleText = "Mari lepak";

                                                if (name != "") {
                                                  print(mtoken);
                                                }
                                              },
                                              child: const Icon(
                                                CupertinoIcons.hand_raised_fill,
                                                color: CupertinoColors.black,
                                                size: 50,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )));
                        },
                      ),
                    );
                  }
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Container(
                decoration: BoxDecoration(
                  color: CupertinoTheme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(25),
                ),
                width: 150,
                height: 36,
                child: SizedBox(
                  child: CupertinoButton(
                    disabledColor: CupertinoColors.inactiveGray,
                    padding: EdgeInsets.zero,
                    onPressed: () async {
                      Navigator.pop(context);
                    },
                    child: const Text('Back',
                        style: TextStyle(color: CupertinoColors.white)),
                  ),
                ),
              ),
            ),
          ])),
    ));
  }
}
