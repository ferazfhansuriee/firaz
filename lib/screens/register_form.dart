import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/services/auth_service.dart';
import 'package:jom/services/database.dart';
import 'package:jom/services/notification.dart';
import '../global.dart' as global;
import '../models/user.dart';

import '../utils/progress_dialog.dart';
import '../utils/toast.dart';
import '../widgets/forms/phone_no_input.dart';
import '../widgets/forms/text_input.dart';

class RegisterFormScreen extends StatefulWidget {
  const RegisterFormScreen({Key? key}) : super(key: key);

  @override
  _RegisterFormScreenState createState() => _RegisterFormScreenState();
}

class _RegisterFormScreenState extends State<RegisterFormScreen> {
  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneNoController = TextEditingController();
  final _passwordController = TextEditingController();
  PlatformFile? pickedFile;
  UploadTask? uploadTask;
  int totalBuds = 7;
  int bud = 1;
  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNoController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Future uploadFile(String uid) async {
    final path = 'images/userDP/$uid';
    final file = File(pickedFile!.path!);
    final ref = FirebaseStorage.instance.ref().child(path);
    uploadTask = ref.putFile(file);
    final snapshot = await uploadTask!.whenComplete(() {});
    final urlDownload = await snapshot.ref.getDownloadURL();
    print(urlDownload);
  }

  showUpload(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = CupertinoButton(
      child: Text(
        'Cancel',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = CupertinoButton(
      child: Text(
        'Confirm',
        style: const TextStyle(
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Confirm Image",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: CupertinoTheme.of(context).primaryColor,
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(onWillPop: () => Future.value(false), child: alert);
      },
    );
  }

  Future selectFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.image);
    setState(() {
      pickedFile = result!.files.first;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: CupertinoPageScaffold(
        navigationBar: const CupertinoNavigationBar(
          middle: Text('User Registration'),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).padding.top + 44),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Column(
                      children: [
                        TextInput(
                            prefixWidth: 85,
                            label: 'Name',
                            controller: _nameController,
                            maxLength: 255,
                            required: true),
                        const Divider(
                            color: CupertinoColors.systemGrey5, height: 1),
                        TextInput(
                            prefixWidth: 85,
                            label: 'Email',
                            controller: _emailController,
                            type: 'email',
                            maxLength: 255),
                        const Divider(
                            color: CupertinoColors.systemGrey5, height: 1),
                        TextInput(
                            prefixWidth: 85,
                            label: 'Password.',
                            controller: _passwordController,
                            type: 'password',
                            maxLength: 20,
                            required: true),
                        const Divider(
                            color: CupertinoColors.systemGrey5, height: 1),
                        Container(
                          color: Colors.white,
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  color: Colors.white,
                                  padding: const EdgeInsets.all(5),
                                  child: Text(
                                    "Select Bud",
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 85,
                                color: Colors.white,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    physics:
                                        const AlwaysScrollableScrollPhysics(),
                                    itemCount: totalBuds,
                                    itemBuilder: (context, index) {
                                      int num = index + 1;
                                      return GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              bud = num;
                                            });

                                            print(bud);
                                          },
                                          child: Image.asset(
                                            'assets/images/bud$num.png',
                                            height: 15,
                                          ));
                                    }),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  color: Colors.white,
                                  padding: const EdgeInsets.all(5),
                                  child: Text(
                                    "Bud $bud",
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    decoration: const BoxDecoration(
                      color: CupertinoColors.white,
                      border: Border(
                        bottom: BorderSide(color: CupertinoColors.systemGrey5),
                      ),
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      height: 36,
                      child: CupertinoButton(
                        color: CupertinoTheme.of(context).primaryColor,
                        disabledColor: CupertinoColors.inactiveGray,
                        padding: EdgeInsets.zero,
                        onPressed: () async {
                          _submitForm(context);
                        },
                        child: const Text('Sign Up',
                            style: TextStyle(color: CupertinoColors.white)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _submitForm(BuildContext context) async {
    String name = _nameController.text;

    List<String> errors = [];
    if (name.isEmpty) errors.add('Enter username');

    if (errors.isEmpty) {}

    if (errors.isNotEmpty) {
      Toast.show(context, 'danger', errors[0]);
    }
  }
}
