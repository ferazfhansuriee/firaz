import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jom/services/database.dart';
import 'package:jom/utils/toast.dart';
import 'package:roundcheckbox/roundcheckbox.dart';

import '../widgets/search_bar.dart';

class NotificationScreen extends StatefulWidget {
  String? notiId;
  NotificationScreen({Key? key, this.notiId}) : super(key: key);

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final _auth = FirebaseAuth.instance;
  bool value = false;
  String firstName = '';
  String googleName = '';
  String? groupName;
  String? activityName;
  String uid = '';
  String utoken = "";
  List<dynamic> friends = [];
  String id = "";
  List<int> friendIdList = [];
  String friendUrl = "";
  String friendId = "";
  List<dynamic> notifications = [];
  List<dynamic> activities = [];
  String url = "";
  String notiId = "";

  @override
  void initState() {
    final User user = _auth.currentUser!;
    uid = user.displayName.toString();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: RefreshIndicator(
            color: Colors.black,
            onRefresh: _refreshContent,
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                padding:
                    EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20, bottom: 5, left: 20, right: 20),
                        child: Container(
                            child: SearchBar(onSubmitted: (value) => {})),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Your Notifications",
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 25,
                            ),
                          ),
                        ),
                      ),
                      StreamBuilder<QuerySnapshot>(
                        stream: FirebaseFirestore.instance
                            .collection("notification")
                            .where("reciever_name", isEqualTo: uid)
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return const Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.black),
                              ),
                            );
                          } else {
                            final docs = snapshot.data!.docs;

                            return Padding(
                              padding: const EdgeInsets.only(bottom: 10.0),
                              child: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height + 85,
                                  width: double.infinity,
                                  child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 3),
                                      child: ListView.builder(
                                          physics:
                                              AlwaysScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          itemCount: docs.length,
                                          itemBuilder: (context, index) {
                                            print(docs.length);
                                            String receive =
                                                docs[index]['reciever_name'];
                                            String sender =
                                                docs[index]['sender_name']!;

                                            String message =
                                                docs[index]['message'];
                                            String receiveToken =
                                                docs[index]['reciever_token'];
                                            if (message == "Member Request") {
                                              groupName =
                                                  docs[index]['group_name'];
                                            } else if (message ==
                                                "Activity Request") {
                                              groupName = docs[index]['gName'];
                                              print(groupName);
                                              activityName =
                                                  docs[index]['adName'];
                                            }
                                            print(firstName);
                                            print(googleName);
                                            return Padding(
                                              padding: const EdgeInsets.all(5),
                                              child: Card(
                                                child: Container(
                                                  padding: EdgeInsets.all(10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceEvenly,
                                                        children: [
                                                          Column(
                                                            children: [
                                                              if (receive !=
                                                                      firstName &&
                                                                  receive !=
                                                                      googleName)
                                                                Text(receive),
                                                              if (sender !=
                                                                      firstName &&
                                                                  sender !=
                                                                      googleName)
                                                                Text(sender),
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .all(3),
                                                                child: Text(
                                                                  message,
                                                                  style: TextStyle(
                                                                      color: Color
                                                                          .fromARGB(
                                                                              255,
                                                                              0,
                                                                              0,
                                                                              0)),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Visibility(
                                                            replacement:
                                                                Container(),
                                                            visible: (sender !=
                                                                    firstName &&
                                                                sender !=
                                                                    googleName),
                                                            child:
                                                                FloatingActionButton(
                                                                    mini: true,
                                                                    backgroundColor:
                                                                        Colors
                                                                            .white,
                                                                    onPressed:
                                                                        () async {
                                                                      if (message ==
                                                                          "Friend Request") {
                                                                        _acceptFriend(
                                                                            sender,
                                                                            receive,
                                                                            receiveToken,
                                                                            message,
                                                                            docs[index].id);
                                                                      } else if (message ==
                                                                          "Member Request") {
                                                                        _acceptMember(
                                                                            sender,
                                                                            receive,
                                                                            receiveToken,
                                                                            message,
                                                                            groupName!,
                                                                            docs[index].id);
                                                                      } else if (message ==
                                                                          "Activity Request") {
                                                                        _acceptActivity(
                                                                            sender,
                                                                            receive,
                                                                            receiveToken,
                                                                            message,
                                                                            groupName!,
                                                                            activityName!,
                                                                            docs[index].id);
                                                                      }
                                                                    },
                                                                    child: Icon(
                                                                      CupertinoIcons
                                                                          .hand_raised,
                                                                      color: Colors
                                                                          .black,
                                                                      size: 20,
                                                                    )),
                                                          ),
                                                          Visibility(
                                                            replacement:
                                                                Container(
                                                              width: 85,
                                                            ),
                                                            visible: (sender !=
                                                                    firstName &&
                                                                sender !=
                                                                    googleName),
                                                            child:
                                                                FloatingActionButton(
                                                                    mini: true,
                                                                    backgroundColor:
                                                                        Colors
                                                                            .white,
                                                                    onPressed:
                                                                        () async {
                                                                      _denyNotification(
                                                                          docs[index]
                                                                              .id);
                                                                    },
                                                                    child: Icon(
                                                                      CupertinoIcons
                                                                          .hand_raised_slash,
                                                                      color: Colors
                                                                          .black,
                                                                      size: 20,
                                                                    )),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            );
                                          }))),
                            );
                          }
                        },
                      )
                    ])))));
  }

  Future<void> _refreshContent() async {
    setState(() {
      notifications.clear();
    });
  }

  void _acceptMember(String sender, String receive, String token,
      String message, String groupName, String id) {
    String desc = "";
    String gurl = "";
    print(groupName);
    FirebaseFirestore.instance
        .collection("group")
        .where("group_name", isEqualTo: groupName)
        .get()
        .then((querySnapshot) async {
      if (!querySnapshot.docs.isEmpty) {
        setState(() {
          gurl = querySnapshot.docs[0].get("url");
        });
        await DatabaseService().addGroup(receive, groupName, gurl);
        await DatabaseService().addMember(groupName, receive, token);
        await DatabaseService().deleteNotification(id);
      }
    });

    _refreshContent();
  }

  Future<void> _acceptActivity(String sender, String receive, String token,
      String message, String gName, String aName, String id) async {
    print(gName);
    await DatabaseService().updateJoin(aName, sender);
    await DatabaseService().addActivityMember(aName, sender, receive);
    await DatabaseService().deleteNotification(id);
    _refreshContent();
  }

  Future<void> _acceptFriend(String sender, String receive, String token,
      String message, String id) async {
    await DatabaseService().addFriend(
      sender,
      receive,
      token,
    );
    await DatabaseService().addFriend2(
      sender,
      receive,
      token,
    );
    await DatabaseService().deleteNotification(id);
    _refreshContent();
  }

  Future<void> _denyNotification(String id) async {
    await DatabaseService().deleteNotification(id);
    _refreshContent();
  }
}
