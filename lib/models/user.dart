import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  String? uid;
  String? email;
  String? name;
  String? phoneNo;
  String? password;
  String? photoUrl;
  int? friends;

  User(
      {this.uid,
      this.email,
      this.name,
      this.phoneNo,
      this.password,
      this.photoUrl,
      this.friends});
  Stream<User?>? dataListFromSnapshot(QuerySnapshot querySnapshot) {
    querySnapshot.docs.map((snapshot) {
      final Map<String, dynamic> dataMap =
          snapshot.data() as Map<String, dynamic>;

      return User(
        uid: dataMap['uid'],
        name: dataMap['name'],
      );
    });
  }
}
