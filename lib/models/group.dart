class Group {
  String? id;
  String? name;
  String? url;
  List<Map<String, dynamic>>? members;
  Group({this.id, this.name, this.url, this.members});
  factory Group.fromJson(Map<String, dynamic> json) {
    return Group(
      id: json['id'],
      name: json['name'],
      url: json['url'],
      members: json['members'],
    );
  }
}
