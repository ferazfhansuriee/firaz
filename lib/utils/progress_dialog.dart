import 'package:flutter/cupertino.dart';
import '../widgets/custom_progress_indicator.dart';

class ProgressDialog {
  static Future<void> show(BuildContext context, GlobalKey key) async {
    return showCupertinoDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Container(
          key: key,
          child: const CustomProgressIndicator(),
        );
      }
    );
  }

  static void hide(GlobalKey key) {
    Navigator.of(key.currentContext!, rootNavigator: true).pop();
  }
}
