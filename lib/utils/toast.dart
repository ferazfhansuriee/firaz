import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Toast {
  static void show(BuildContext context, String type, String message,
      {int? duration, double? offset}) {
    Color _color = Colors.grey[900]!;
    Color _borderColor = Colors.grey[500]!;
    Color _backgroundColor = Colors.grey[400]!;

    if (type == 'success') {
      _color = Color.fromARGB(255, 58, 126, 62);
      _borderColor = Colors.green[500]!;
      _backgroundColor = Color.fromARGB(255, 255, 255, 255);
    } else if (type == 'danger') {
      _color = Colors.red[900]!;
      _borderColor = Colors.red[500]!;
      _backgroundColor = Color.fromARGB(255, 255, 255, 255);
    }

    Widget toast = SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        margin: EdgeInsets.only(bottom: (offset != null) ? offset : 10),
        decoration: BoxDecoration(
          border: Border.all(color: _borderColor),
          borderRadius: BorderRadius.circular(12),
          color: _backgroundColor,
        ),
        child: Text(message, style: TextStyle(fontSize: 18, color: _color)),
      ),
    );

    FToast fToast = FToast();
    fToast.init(context);
    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: (duration != null) ? duration : 2),
    );
  }
}
